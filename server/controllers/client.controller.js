const ZonaModel = require('../models/zone.model');
const UserModel = require('../models/user.model');
const Model = require(`../models/client.model`);

module.exports = {
    /**
     * @param model of Client
     * @param from number from record wants to start
     * @param value string value to search
     * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
     * @param status number Get Records 1 = Active 2 = Inactive 3 = both
     * @return object {Status, Result, Data, TotalRecords, [error]}
     */
    getAll: async(req, res, next) => {
        try {
            let limit = Number(req.query.limit || 10);
            let skip = Number(req.query.from || 0);
            if (req.query.value.length > 0) {
                filters = { $text: { $search: req.query.value } };
            } else {
                filters = {};
            }

            if (req.query.status == 1) {
                filters.status = true;
            } else if (req.query.status == 2) {
                filters.status = false;
            }
            const zones = await UserModel.findById(req.user._id);
            if (!zones) {
                res.status(204).json({
                    status: 204,
                    result: false,
                    err: {
                        message: `El usurio ${ req.user.name } ${ req.user.lastaname } no tiene asignado ninguna Zona`
                    }
                });
            } else {
                filters.zone = { $in: zones.zone };
                console.log(zones.zone);
            }
            console.log(filters);
            const data = await Model.find(filters)
                .limit(limit)
                .skip(skip)
                .populate({
                    path: 'city zone',
                    populate: {
                        path: 'state'
                    },
                });

            res.status(200).json({
                status: 200,
                result: true,
                total: await (Model.count(filters)),
                data: data,
            });

        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },
     /**
     * Get a record from entity by Id
     * @param id strign of record
     * @return object {Status, Result, Data, [error]}
     */
    getById: async(req, res, next) => {
        try {
            const { id } = req.params;
            const data = await Model.findById(id,{user: 0})
                .populate({
                path: 'city zone',
                select: {name: 1, _id: 0},
                populate: {
                    path: 'state',
                    select: {name: 1, _id: 0},
                },
            });
            if (!Model) {
                res.status(400).json({
                    status: 400,
                    result: false,
                    err: {
                        message: 'ID no existe'
                    }
                });
            } else {
                res.status(200).json({
                    status: 200,
                    result: true,
                    data: data
                });
            }
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }

    },
}