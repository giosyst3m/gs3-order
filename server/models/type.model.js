const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    code: {
        type: Number,
        required: true
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});

ModelSchema.index({ code: 1 }, { unique: true });
ModelSchema.index({ name: 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Type', ModelSchema);