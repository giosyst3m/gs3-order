const path = require('path');
const fs = require('fs');
var colors = require('colors');
var LogModel = require('../models/log.model');

let getMonthName = async(date) => {
    var objDate = new Date(date),
        locale = "en-us",
        month = objDate.toLocaleString(locale, { month: "long" });
    return month;
}


let deleteFiles = async(file, folder = '../../') => {

    let _file = path.resolve(__dirname, folder + file);
    if (fs.existsSync(_file)) {
        fs.unlinkSync(_file);
        console.log('Delete File:', _file.red)
    }


}

let saveLog = async(code, message, entity, id, user, type) => {
    data = await LogModel.create({ code, message, entity, id, user, type });
    if (data) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    getMonthName,
    deleteFiles,
    saveLog
}