const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    name: {
        type: String,
        default: '_GUEST_',
        required: true
    },

    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});
ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Role', ModelSchema);