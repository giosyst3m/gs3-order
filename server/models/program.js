const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;


module.exports = mongoose.model('Program', new Schema({
    name: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    description: {
        type: String,
    },
    status: {
        type: Boolean,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
}));