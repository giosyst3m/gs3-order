const express = require('express');
const router = require('express-promise-router')();
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const path = 'tax';
const Controller = require(`../controllers/${ path }`);


router.route(`/${ path }/current`)
    .get(Controller.getCurrent);

module.exports = router;