const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/comment.controller.`);
const app = express();

let Comment = require('../models/comment.model');

app.get('/comment/:type/:row', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/comment.model`);
    Controller.getAll(req, res, next, model);

});



module.exports = app;