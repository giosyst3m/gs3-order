const ProductModel = require('../models/product.model');
const LineModel = require('../models/line.model');
const CategoryModel = require('../models/category.model');
var fs = require('fs');
var pdf = require('html-pdf');
var cloudinary = require('cloudinary');
const nodemailer = require('nodemailer');
var { getPDFHeader, getPDFooter } = require('../helpers/pdf.helper');

module.exports = {
    productToExcel: async(req, res, next) => {
        try {
            console.log('START: ' + 'Product: '.cyan + 'Export EXCEL');
            let _product = await ProductModel.find({
                    status: true,
                    stock: { $gt: 0 },
                    line: {
                        $in: req.body.lines
                    }
                }, {
                    barcode: 1,
                    sku: 1,
                    name: 1,
                    price: 1,
                    stock: 1,
                    _id: 0
                })
                .populate({
                    path: 'brand category line',
                    select: { _id: 0, name: 1 }
                })

            data = [];
            _product.forEach(element => {
                data.push({
                    "Código de Barra": element.barcode,
                    "REF": element.sku,
                    "Nombre": element.name,
                    "Marca": element.brand == null ? 'Sin Marca' : element.brand.name,
                    "Línea": element.line == null ? 'Sin Línea' : element.line.name,
                    "Categoía": element.category == null ? 'Sin Caregoría' : element.category.name,
                    "Cantidad": element.stock,
                    "Precio": element.price
                });
            });
            console.log('END: ' + 'Product: '.cyan + 'Export EXCEL');
            return res.json({
                status: 200,
                result: true,
                data: data,
                pro: _product,
            });
        } catch (error) {
            return res.status(500).json({
                status: 500,
                result: true,
                error
            });
        }   
       


    },
    productToPDF: async(req, res, next) => {
        req.params.img = req.params.img === 'true' ? true : false;
        console.log('START: ' + 'Product: '.cyan + 'Export PDF:', req.params.img ? 'with imagen'.green : 'without Imagen'.green);
        line = await ProductModel.distinct('line', { status: true, stock: { $gt: 0 }, line: { $in: req.body.lines } });

        data = [];
        for (let index = 0; index < line.length; index++) {
            const element = line[index];
            category = await ProductModel.distinct('category', { status: true, stock: { $gt: 0 }, line: element });
            data2 = [];
            for (let index2 = 0; index2 < category.length; index2++) {
                const elementCat = category[index2];
                product = await ProductModel.find({ status: true, stock: { $gt: 0 }, line: element, category: elementCat }, {
                        barcode: 1,
                        sku: 1,
                        name: 1,
                        price: 1,
                        _id: 0,
                        img_url: 1,
                        img: 1,
                        sticker_sell: 1,
                        sticker_color: 1,
                    })
                    .populate({
                        path: 'brand',
                        select: { _id: 0, name: 1 }
                    });
                data2.push({
                    category: await CategoryModel.findById(elementCat, { name: 1, _id: 0 }),
                    product: product

                })
            }
            data.push({
                line: await LineModel.findById(element, { name: 1, _id: 0 }),
                category: data2

            });
        }


        html = '';
        if (req.params.img) {
            html += `<html>
            <head>
            <style>
                body {
                    color: #73879C;
                    font-family: "Helvetica Neue",Roboto,Arial,"Droid Sans",sans-serif;
                    font-size: 10px !important;
                    font-style: normal; 
                    line-height: 1.471;
                }
                .center {
                    text-align: center;
                    font-style: normal; 
                }
                table {
                    width: 100%;
                    max-width: 100%;
                    margin-bottom: 0px;
                    border-collapse: collapse;
                    font-size: 10px !important;
                    font-style: normal; 
                }
                .titulo_producto{
                    color: #4682B4;
                    font-size: 10px;
                    font-style: normal; 
                    line-height: 10px;
                    }
                .codigo{
                        font-family:Arial, Helvetica, sans-serif;
                        color:#808080 !important;
                        font-size:10px;
                        font-style: normal; 
                        position: relative;
                        top: -10px;
                    }

                    .precio{
                            font-family: Arial, Helvetica, sans-serif;
                            color: #22487E !important;
                            font-size: 10px;
                            font-style: normal; 
                        }
                    .border {
                        border: solid #ddd;
                        border-width: thin;
                        font-style: normal; 
                    }
                    .linea {
                        text-align: center;
                        font-size: 12px;
                        font-style: normal; 
                    }
                    .referencia {
                        font-family: Arial, Helvetica, sans-serif;
                        color: #22487E !important;
                        font-size: 10px;
                        font-style: normal; 
                    }
            </style></head>
            `;
            html += `<body>`;
            htmlHeader = await getPDFHeader(req.query.company, false);
            var row = 1;
            var col = 1;
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                titulo = `${ htmlHeader } <table class="center" style="width:100%; font-size: 12px;"><tr><td><b>${ element.line.name }</b></td></tr></table><br>`;
                for (let index2 = 0; index2 < element.category.length; index2++) {
                    const cat = element.category[index2];
                    for (let index3 = 0; index3 < cat.product.length; index3++) {
                        if (row == 1) {
                            html += titulo;
                            html += `<table width="100%" style="width:100%">`;
                        }
                        const prod = cat.product[index3];
                        if (col == 1) {
                            html += `<tr>`;
                        }
                        html += `<td width="33.33%" class="border" height="75"> 
                                <table>
                                    <tr>
                                        <td rowspan="2">` +
                                            cloudinary.image(prod.img, { width: 75, height: 75, crop: "scale" }) +
                                        `</td>
                                        <td class="titulo_producto center">
                                            ${ prod.name }<br>
                                            <b>${ prod.brand == null ? 'Sin Marca': prod.brand.name }</b>
                                            <span class="codigo center"><br><br><br>Ref. ${ prod.sku }</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                    </tr>
                                    <tr>
                                        <td class="center referencia">Cod. ${ prod.barcode }</td>
                                        <td class="precio center">${ prod.price.toLocaleString('en', {   
                                                minimumFractionDigits: 0,
                                                maximumFractionDigits: 0,
                                            })}`;
                                            if (prod.sticker_sell) {
                                                html += `<br><span style="background:${ prod.sticker_color };color:#fff;padding:0px 5px 0px 5px;font-size: 8px;">${ prod.sticker_sell }</span>`
                                            }
                        html += `       </td>
                                    </tr>
                                </table>
                        </td>`;
                        col++;
                        if (col > 3) {
                            html += `</tr>`;
                            col = 1;
                        }
                        row++;
                        if (row > 18) {
                            row = 1;
                            html += ` </table><div style="page-break-after:always;"></div>`;
                        }
                    }
                }
            }
            html += `
            </body></html>`;
            var options = {
                format: 'Letter',
                paginationOffset: 1, // Override the initial pagination number
                'header': {
                    'height': '0px',
                },
                'footer': {
                    'height': '0px',
                }
            };
        } else {
            html = `<html>
                        <head>
                            <style>
                                td, th {
                                    padding: 3px;
                                    font-size: 13px;
                                    font-family: 'Arial'; 
                                    color:#73879C 
                                }
                                tbody, td {
                                    border: 1px solid #ccc;
                                    font-weight: bold color: #73879C;
                                    font-size: 10px
                                }
                            </style>
                        </head>
                        <body>
                        ${await getPDFHeader(req.query.company)}
                            <table class="center" style="width:100%; font-size: 12px;">
                                <tbody>`;
                                    for (let index = 0; index < data.length; index++) {
                                        const element = data[index];
                                        html += `<tr style="text-align: center;"><td colspan="5">${ element.line.name }</td></tr>`;
                                        for (let index2 = 0; index2 < element.category.length; index2++) {
                                            const cat = element.category[index2];
                                            html += `<tr style="text-align: center;"><td colspan="5">${ cat.category.name }</td></tr>`;
                                            for (let index3 = 0; index3 < cat.product.length; index3++) {
                                                const prod = cat.product[index3];
                                                html += `<tr>
                                            <td>${ prod.barcode }</td>
                                            <td>${ prod.sku }</td>
                                            <td>${ prod.name } <span style="background:${ prod.sticker_color };color:#fff;padding:0px 5px 0px 5px">${ prod.sticker_sell }</span></td>
                                            <td>${ prod.brand == null ? 'Sin Categoría': prod.brand.name }</td>
                                            <td style="text-align: right;">${ prod.price }</td>
                                            </tr>`;
                                            }
                                        }
                                    }
            html += `          </tbody>
                            </table>
                        </body>
                    </html>`;
                    var options = {
                        format: 'Letter',
                        paginationOffset: 1, // Override the initial pagination number
                        'header': {
                            'height': '80px',
                            "contents": ''
                        },
                        'footer': {
                            'height': '30px',
                            "contents": {
                                default: await getPDFooter('Catálogo de productos ' + (req.params.img ? 'con imagen' : 'sin Imagen'))
                            }
            
                        }
                    };
        }
        
        pdf.create(html, options).toFile('./public/pdf/product-no-img.pdf', async function(err, file) {
            // Upload Cloudinary
            _cloudinary = await cloudinary.uploader.upload(file.filename, function(result) {
                return result;
            }, { resource_type: "raw", folder: 'export/product/pdf', tags: ['export ', 'product', 'pdf'] });

            if (err) return console.log(err);

            if (req.params.email !== 'no-email') {
                //Send Email to notify Beging process Excel File
                // Generate test SMTP service account from ethereal.email
                // Only needed if you don't have a real mail account for testing
                nodemailer.createTestAccount((err, account) => {

                    // create reusable transporter object using the default SMTP transport
                    let transporter = nodemailer.createTransport({
                        host: process.env.mailHost,
                        port: process.env.mailPort,
                        secure: process.env.mailSecure, // true for 465, false for other ports
                        auth: {
                            user: process.env.mailUser, // generated ethereal user
                            pass: process.env.mailPass // generated ethereal password
                        }
                    });
                    // setup email data with unicode symbols
                    let mailOptions = {
                        from: process.env.mailFrom, // sender address
                        to: `giosyst3m@gmail.com`, // list of receivers
                        cc: req.params.email,
                        subject: 'Catalogo de Productos sin imagen para descargar', // Subject line
                        text: 'Se inicia el Procesos de Productos y actualización de inventario, ', // plain text body
                        html: `<b>Pedidos OnLine</b><p>Se generó un PDF <a href="${ _cloudinary.secure_url }"> Clic para descargar el archivo PDF sin imagenes</a> </p> ` // html body
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message sent: %s'.green, info.messageId);
                        // Preview only available when sending through an Ethereal account
                        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
                    });
                });
            }
            console.log('END: ' + 'Product: '.cyan + 'Export: PDF:', req.params.img ? 'with imagen'.green : 'without Imagen'.green); // { filename: '/app/businesscard.pdf' }
            return res.json({
                status: 200,
                result: true,
                url: _cloudinary.secure_url,
                data,
                html: html
            });
        });
    }
}