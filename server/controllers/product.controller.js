const Model = require('../models/product.model');

module.exports = {
    /**
     * Get all Product active 
     * @author Giosyst3m
     * @version 1.0.0
     * @param 
     * @returns array all products
     */
    index: async(req, res, next) => {
        // let regex = new RegExp(req.query.keywords, 'i');
        let limit = Number(req.query.limit || 28);
        let skip = Number(req.query.from || 0);
        // let or = [{ name: { $regex: regex } }, { sku: { $regex: regex } }, { barcode: { $regex: regex } }];
        let filters = {
            status: true,
        };

        if (req.query.brand) {
            filters['brand'] = {
                _id: req.query.brand
            }
        }

        if (req.query.line) {
            filters['line'] = {
                _id: req.query.line
            }
        }

        if (req.query.category) {
            filters['category'] = {
                _id: req.query.category
            }
        }

        if (req.query.value.length > 0) {
            filters['$text'] = { $search: req.query.value };
        }

        const data = await Model.find(filters)
            // .or(or)
            .limit(limit)
            .skip(skip)
            .populate('brand', 'name')
            .populate('type', 'name')
            .populate('line', 'name')
            .populate('condition', 'name')
            .populate('category', 'name')

        res.status(200).json({
            status: 200,
            result: true,
            total: await (Model.count(filters)),
            data: data,
        });
    },
    /**
     * Get Prodcut by Barcode
     * @author Giosyst3m
     * @version 1.0.0
     * @param barcode string
     * @returns one record product
     */
    getByBarcode: async(req, res, next) => {
        Model.findOne({ barcode: req.params.barcode, status: true, stock: { $gt: 0 } }, (error, data) => {
            if (error) {
                return res.status(500).json({
                    status: 500,
                    result: false,
                    error
                });
            }
            if (!data) {
                return res.status(404).json({
                    status: 404,
                    result: false,
                    error: { message: `No se encontró el Producto con el Código de barra ${ req.params.barcode}` }

                });
            }
            return res.status(200).json({
                status: 200,
                result: true,
                data: data
            });
        });
    },


}