const express = require('express');

const app = express();

app.use(require('./product.router'));
app.use(require('./upload'));
app.use(require('./images'));
app.use(require('./client.route'));
app.use(require('./document.router'));
app.use(require('./tax'));
app.use(require('./comment.router'));
app.use(require('./inventory.router'));
app.use(require('./delivery.route'));
app.use(require('./dashboard.router'));
app.use(require('./upload-imagen'));
app.use(require('./upload-bill'));
app.use(require('./report.router'));
app.use(require('./user.router'));
app.use(require('./log.router'));
app.use(require('./entity.route'));

module.exports = app;