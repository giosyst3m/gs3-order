const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    code: {
        type: String,
        required: true,
    },
    correlative: {
        type: String,
        required: true,
        default: 0,
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: Schema.Types.ObjectId,
        ref: 'City',
        required: true
    },
    phone: {
        type: String,
        required: false
    },
    mobil: {
        type: String,
        required: false
    },
    zone: {
        type: Schema.Types.ObjectId,
        ref: 'Zone',
        required: true
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ code: 1, correlative: 1 }, { unique: true });
ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Client', ModelSchema);