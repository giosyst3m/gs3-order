const mongoose = require('mongoose');
const Schema = mongoose.Schema;

ModelSchema = new Schema({
    excel: {
        type: Number,
        required: true,
    },
    sku: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    stock: {
        type: Number,
        default: 0,
        required: true,
    },
    category: {
        type: Number,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    brand: {
        type: String,
        required: true
    },
    barcode: {
        type: Number,
        required: true,
    },
    condition: {
        type: Number,
        required: true
    },
    line: {
        type: Number,
        required: true
    },
    code: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    img: {
        type: String,
        required: false,
        default: 'no-image.jpg'
    },
    img_url: {
        type: String,
        required: false,
        default: 'https://res.cloudinary.com/giosyst3m/image/upload/v1534113067/no-image.jpg'
    },
    status: {
        type: Number,
        required: true,
        default: 1
    },
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('ProductTMP', ModelSchema);