const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    order: {
        type: Number,
        default: 1,
        required: true
    },
    icon: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    next: {
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: false
    },
    message: {
        type: String,
        required: true
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'Counter',
        required: true
    },
    buttom: [{
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: false
    }],
    showStock: {
        type: Boolean,
        required: false,
        default: false
    },
    edit: {
        type: Boolean,
        required: false,
        default: false
    },
    delivery: {
        type: Boolean,
        required: false,
        default: false
    },
    deleteDetailInZero: {
        type: Boolean,
        required: false,
        default: false
    },
    deleteNoAssigned: {
        type: Boolean,
        required: false,
        default: false
    },
    billAssociated: {
        type: Boolean,
        required: false,
        default: false
    },
    billShow: {
        type: Boolean,
        required: false,
        default: false
    },
    billForm: {
        type: Boolean,
        required: false,
        default: false
    },
    deleteDetailNoInventory: {
        type: Boolean,
        required: false,
        default: false
    },
    applyDiscount: {
        type: Boolean,
        required: false,
        default: false
    },
    validBill: {
        type: Boolean,
        required: false,
        default: false
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    note: {
        type: String,
        required: false
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    role: [{
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true
    }],
    addProduct: [{
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true
    }],
});

ModelSchema.index({ order: 1, type: 1 }, { unique: true });
ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Status', ModelSchema);