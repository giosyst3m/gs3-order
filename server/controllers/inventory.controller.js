let Document = require('../models/document.model');
let Product = require('../models/product.model');
let Detail = require('../models/detail.model');
var { saveLog } = require('../helpers/lib.helper');

module.exports = {
    /**
     * @param model of Document
     * @param product string Product ID
     * @return object {Status, Result, Data, TotalRecords, [error]}
     */
    getByProduct: async(req, res, next, Model) => {
        try {
            let body = req.body;
            body.quantity = Number(body.quantity);
            body.required = Number(body.required);

            Product.findById({ _id: body.product })
                .exec((err, prod) => {
                    if (err) {
                        return res.status(500).json({
                            status: 5001,
                            result: false,
                            err
                        });
                    }

                    if (!prod) {
                        return res.status(404).json({
                            status: 404,
                            result: false,
                            err: {
                                message: `El Producto ${ body.product } no existe `
                            }
                        });
                    }

                    Detail.findById({ _id: body.detail })
                        .exec((errors, det) => {
                            if (errors) {
                                return res.status(500).json({
                                    status: 5004,
                                    result: false,
                                    errors
                                });
                            }

                            if (det.inventory) {
                                prod.stock = prod.stock + body.quantity;
                            }

                            if (prod.stock <= 0) {
                                return res.status(406).json({
                                    status: 406,
                                    result: false,
                                    err: {
                                        message: `El Sotck del Producto es  ${ prod.stock } no hay unidades disponibles`
                                    }
                                });
                            }

                            if (prod.stock < body.required) {
                                return res.status(406).json({
                                    status: 406,
                                    result: false,
                                    err: {
                                        message: `La cantidad solicitada ${ body.required } es mayor a lo disponible ${ prod.stock }`
                                    }
                                });
                            }

                            if (body.type) {
                                prod.stock = prod.stock + body.required;
                            } else {
                                prod.stock = prod.stock - body.required;
                            }





                            let inv = false;
                            if (body.required > 0) {
                                inv = true;
                            }

                            Detail.findByIdAndUpdate(body.detail, { quantity: body.required, required: body.required, inventory: inv }, { new: true, runValidators: true }, (errors, data) => {
                                if (errors) {
                                    return res.status(500).json({
                                        status: 5005,
                                        result: false,
                                        errors
                                    });
                                }

                                Product.findByIdAndUpdate(body.product, { stock: prod.stock }, { new: true, runValidators: true }, (errors, data) => {

                                    if (errors) {
                                        return res.status(500).json({
                                            status: 5002,
                                            result: false,
                                            errors
                                        });
                                    }

                                    if (!data) {
                                        return res.status(400).json({
                                            status: 400,
                                            result: false,
                                            data
                                        });
                                    }
                                    body['user'] = req.user._id;
                                    let mdoel = new Model(body);
                                    mdoel.save(async(errors, body) => {
                                        if (errors) {
                                            return res.status(500).json({
                                                status: 5003,
                                                result: false,
                                                errors
                                            });
                                        }
                                        message = `<b>Código: </b>${ data.barcode }<br><b>Ref: </b>${ data.sku }<br><b>Producto: </b>${ data.name }<br><b>Solicitadas: </b>${ body.quantity }<br><b>Asginadas: </b>${ body.required }`;
                                        await saveLog(200, message, 'document', body.document, req.user._id, 'Update | Despacho');
                                        res.json({
                                            status: 200,
                                            result: true,
                                            data: body
                                        });


                                    });


                                });
                            });
                        });

                });

        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },
}