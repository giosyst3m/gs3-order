const mongoose = require('mongoose')
const Schema = mongoose.Schema;


module.exports = mongoose.model('Menu', new Schema({
    name: {
        type: String,
        required: [true, 'Nombre Requerido']
    },
    link: {
        type: String,
        required: [true, 'El link es requerido']
    },
    icon: {
        type: String,
    },
    order: {
        type: Number,
        required: true,
        default: 1
    },
    shortcut: {
        type: Boolean,
        required: false,
        default: true
    },
    program: {
        type: Schema.Types.ObjectId,
        ref: 'Program',
        required: true
    },
    target: {
        type: Number,
        default: null
    },
    submenu: {
        ame: {
            type: String,
            required: [true, 'Nombre Requerido']
        },
        link: {
            type: String,
            required: [true, 'El link es requerido']
        },
        icon: {
            type: String,
        },
        order: {
            type: Number,
            required: true,
            default: 1
        },
        shortcut: {
            type: Boolean,
            required: false,
            default: true
        },
        program: {
            type: Schema.Types.ObjectId,
            ref: 'Program',
            required: true
        },
        target: {
            type: Number,
            default: null
        },
        status: {
            required: true,
            default: true,
            type: Boolean
        },
        submenu: {
            ame: {
                type: String,
                required: [true, 'Nombre Requerido']
            },
            link: {
                type: String,
                required: [true, 'El link es requerido']
            },
            icon: {
                type: String,
            },
            order: {
                type: Number,
                required: true,
                default: 1
            },
            shortcut: {
                type: Boolean,
                required: false,
                default: true
            },
            program: {
                type: Schema.Types.ObjectId,
                ref: 'Program',
                required: true
            },
            target: {
                type: Number,
                default: null
            },
            status: {
                required: true,
                default: true,
                type: Boolean
            }
        }
    },
    status: {
        required: true,
        default: true,
        type: Boolean
    }
}));