const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    role: {
        ref: 'Role',
        type: String,
        required: true
    },
    resource: {
        type: String,
        required: true
    },
    action: {
        type: String,
        required: true
    },
    attributes: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});

ModelSchema.index({ role: 1, resource: 1, action: 1 }, { unique: true });
ModelSchema.index({ "$**": 'text' });

module.exports = mongoose.model('Access', ModelSchema);