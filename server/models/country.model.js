const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let countrySchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, 'Nombre Requerido']
    },
    status: {
        required: true,
        default: true,
        type: Boolean
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});


module.exports = mongoose.model('Country', countrySchema);