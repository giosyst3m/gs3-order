const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/product.controller`);
const app = express();

app.get('/product/barcode/:barcode', [verificaToken, validAccess], (req, res, next) => {
    Controller.getByBarcode(req, res, next);
});

app.get('/catalog', [verificaToken, validAccess], (req, res, next) => {
    Controller.index(req, res, next);
});

module.exports = app;