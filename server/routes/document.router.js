const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/document.controller`);
const app = express();

app.get('/document/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getById(req, res, next);
});
app.put('/document/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.updateById(req, res, next);
});

app.get('/document/number/:number/:doc', [verificaToken, validAccess], (req, res, next) => {
    Controller.getByNumber(req, res, next);
});

app.get('/document/totals/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getTotal(req, res, next);
});

app.get('/document/client/:client/:type/:status/:company', [verificaToken, validAccess], (req, res, next) => {
    Controller.getDocByClient(req, res, next);
});

app.post('/document/header/:type/:status', [verificaToken, validAccess], (req, res, next) => {
    Controller.createHeader(req, res, next);
});

app.get('/document/relate/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getDocRelateById(req, res, next);
});

app.post('/document/relate/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.addDocRelate(req, res, next);
});

app.get('/document/detail/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getDetail(req, res, next);
});

app.post('/document/detail/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.createDetail(req, res, next);
});

app.delete('/document/detail/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.deleteDetailInZero(req, res, next);
});

app.patch('/document/pdf/:id/:doc', [verificaToken, validAccess], (req, res, next) => {
    Controller.getPDFByID(req, res, next);
});

app.post('/document/status', [verificaToken, validAccess], (req, res, next) => {
    Controller.getByStatus(req, res, next);
});

module.exports = app;