const CompanyModel = require('../models/company.model');
var cloudinary = require('cloudinary');

let getPDFHeader = async(company, div = true ) => {
    model = await CompanyModel.findById(company);
    if(div){
        return `<div id="pageHeader" style="text-align: center">` +
            cloudinary.image(`client/${ model.folder }/company-logo.png`, { width: 150, crop: "scale" }) +
            cloudinary.image(`client/${ model.folder }/lineas-marcas.png`, { width: 250, crop: "scale" }) +
            `
            <div style="font-size: 8px !important; text: center;color: #73879C;" >
                ${ model.address } | ${ model.phone } | ${ model.email } | ${ process.env.sys_name }
            </div>
            </div>`;
    }else{
        return `<table style="width: 100%;
            max-width: 100%;
            margin-bottom: 0px;
            border-collapse: collapse;" class="center">
                <tr>
                    <td>` +
                        cloudinary.image(`client/${ model.folder }/company-logo.png`, { width: 150, crop: "scale" }) +
                `   </td>
                    <td>` +
                        cloudinary.image(`client/${ model.folder }/lineas-marcas.png`, { width: 250, crop: "scale" }) +
                `   </td>
                </tr>
                <tr>
                    <td style="font-size: 8px !important; text: center;" colspan="2">
                        ${ model.address } | ${ model.phone } | ${ model.email } | <a style="color: #3879C" href="${ process.env.sys_name }">${ process.env.sys_name }</a>
                    </td>
                </tr>
                </table>`;
    }
}

let getPDFooter = async(info) => {
    var currentTime = new Date();
    return `<div style="color: #73879C;font-size: 8px; text-align: center">
    <span style="font-weight: bold;">página {{page}} de {{pages}} |  
    Impreso: ${ currentTime.getDay() }-${ currentTime.getMonth() }-${ currentTime.getFullYear() } ${ currentTime.getHours() }:${ currentTime.getMinutes() }:${ currentTime.getMilliseconds() } | 
    ${ info } | 
    </span> 
    Power By ${ process.env.sys_company  } ${ process.env.sys_url  } ${ process.env.sys_email  }
    </div>`;
}

module.exports = {
    getPDFHeader,
    getPDFooter
}