const Model = require('../models/tax.model');


module.exports = {
    getCurrent: async(req, res, next) => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        let filters = {
            status: true,
            from: { $lte: new Date(yyyy + '-' + mm + '-' + dd) },
            to: { $gte: new Date(yyyy + '-' + mm + '-' + dd) }
        };

        Model.find(filters, '_id')
            .exec((err, data) => {

                if (err) {
                    return res.status(500).json({
                        status: 500,
                        result: true,
                        err
                    });
                }
                let datos = [];
                for (const iterator of data) {
                    datos.push(iterator._id);
                }

                res.json({
                    status: 200,
                    result: true,
                    data: datos
                });

            });
    }

}