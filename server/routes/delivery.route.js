const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/delivery.controller`);
const app = express();


app.post('/delivery/:document', [verificaToken, validAccess], (req, res, next) => {
    Controller.save(req, res, next);
});



module.exports = app;