const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const nodemailer = require('nodemailer');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const fs = require('fs');
const path = require('path');
var cloudinary = require('cloudinary');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


const ProductModel = require('../models/product.model');

/**
 * Upload Multiple Files to Cloudinary and Upadate product's imagen
 * @param Files array images files
 */
app.put('/upload/product/imagen', [verificaToken, validAccess, multipartMiddleware], async(req, res, next) => {

    console.log('START: \x1b[36mProduct\x1b[0m - Upload: Imagen');
    x = Date();
    i = 1;
    var data = [];
    if (!req.files) {
        return res.status(400)
            .json('No se ha seleccionado ningún archivo');
    }


    // Valid extension
    var extensions = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG'];

    for (let index = 0; index < req.files.file.length; index++) {
        const element = req.files.file[index];

        // Get name and Extension file
        var file = req.files.file[index];
        var shortName = file.name.split('.');
        var ext = shortName[shortName.length - 1];

        // Valid extension fiel
        if (extensions.indexOf(ext) < 0) {
            ext = `La extensión del archivo es ${ ext } no es permitido, solo debe subir con extenciones ` + extensions.join(', ');
        } else {
            ext = ext;
        }

        // Serach PRodyct by Barcode from Name Name File
        _product = await ProductModel.findOne({ barcode: shortName[0] });

        if (_product) {
            // // Delete old Images products
            await cloudinary.uploader.destroy(_product.img);

            // Upload Cloudinary
            _cloudinary = await cloudinary.uploader.upload(file.path, function(result) {
                return result;
            }, { folder: 'product', public_id: shortName[0], tags: ['import', 'product', shortName[0]] });

            // // Update product with new Image
            _update = await ProductModel.findByIdAndUpdate(_product._id, { img: _cloudinary.public_id, img_url: _cloudinary.secure_url });
            _data = {
                status: 200,
                result: true,
                file: `${ shortName[0] }.${ ext }`,
                name: shortName[0],
                extesion: ext,
                product: {
                    name: _update.name,
                    sku: _update.sku
                },
                cloudinary: {
                    public_id: _cloudinary.public_id,
                    url: _cloudinary.secure_url
                }
            };
        } else {
            _data = {
                status: 404,
                result: false,
                file: `${ shortName[0] }.${ ext }`,
                name: shortName[0],
                extesion: ext,
                product: {
                    name: 'producto no existente',
                    sku: 'sku no existente'
                },
                cloudinary: {
                    public_id: 'no-image',
                    url: 'https://res.cloudinary.com/giosyst3m/image/upload/v1534113067/no-image.jpg'
                }
            };
        }
        // console.log(i, _data);

        data.push(_data);
        i++;
    }
    await sendEmail(data, req.user);
    console.log('END: \x1b[36mProduct\x1b[0m - Upload: Imagen: \x1b[37m\x1b[41m%s\x1b[0m\x1b[40m ', i);
    console.log(x);
    console.log(Date());
    return res.status(200)
        .json({
            ok: true,
            message: 'Arhivos subidos',
            data: data,
        });

});

function sendEmail(data, user) {
    html = '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr style="color: #fff;background-color: #337ab7;"><td>#</td><td>Actualizado</td><td>Archivo</td><td>Extensión</td><td>Producto</td><td>REF</td><td>Imagen</td></tr></thead>';
    html += '<tbody>';
    i = 1;
    for (let index = 0; index < data.length; index++) {
        const element = data[index];
        html += '<tr>';
        html += `<td>${ i }</td>`;
        html += `<td>${ element.result?'Si':'No' }</td>`;
        html += `<td>${ element.file }</td>`;
        html += `<td>${ element.extesion }</td>`;
        html += `<td>${ element.product.name }</td>`;
        html += `<td>${ element.product.sku }</td>`;
        html += `<td><img src="${ element.cloudinary.url }" width="100px" height="100px"></td>`;
        html += '</tr>';
        i++;
    }
    html += '</tbody></table>';
    //Send Email to notify Beging process Excel File
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: process.env.mailHost,
            port: process.env.mailPort,
            secure: process.env.mailSecure, // true for 465, false for other ports
            auth: {
                user: process.env.mailUser, // generated ethereal user
                pass: process.env.mailPass // generated ethereal password
            }
        });
        // setup email data with unicode symbols

        let mailOptions = {
            from: process.env.mailFrom, // sender address
            to: user.email || process.env.mailFrom, // list of receivers
            subject: 'Importación de Imagenes para Productos Finalizada', // Subject line
            text: 'Se inicia el Procesos de Productos y actualización de inventario, ', // plain text body
            html: '<b>Peodidos OnLine</b><p>Se Finalizó el Procesos de Actualizar las imagenes de los productos, favor hacer la revisión en el sistema </p>' + html // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    });
}
module.exports = app;