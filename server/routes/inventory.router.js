const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/inventory.controller`);
const app = express();



// ============================
// Create a row
// ============================
app.post('/inventory', [verificaToken, validAccess], function(req, res, next) {
    let model = require(`../models/inventory.model`);
    Controller.getByProduct(req, res, next, model);


});

module.exports = app;