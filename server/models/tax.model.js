const mongoose = require('mongoose');
var DateOnly = require('mongoose-dateonly')(mongoose);
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    tax: {
        type: Number,
        required: true
    },
    from: {
        type: Date,
        required: true
    },
    to: {
        type: Date,
        required: true
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Tax', ModelSchema);