const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/user.controller`);
const app = express();

app.post('/user', [verificaToken], (req, res, next) => {
    Controller.create(req, res, next);
});

app.get('/user', [verificaToken], (req, res, next) => {
    Controller.index(req, res, next);
});

app.post('/user/reseat', [verificaToken], (req, res, next) => {
    Controller.resaetPassword(req, res, next);
});

app.patch('/user/reseat', (req, res, next) => {
    Controller.recoveryPassword(req, res, next);
});

app.get('/user/:id', [verificaToken], (req, res, next) => {
    Controller.getById(req, res, next);
});

app.put('/user/:id', [verificaToken], (req, res, next) => {
    Controller.update(req, res, next);
});

app.patch('/user/active/:id', [verificaToken], (req, res, next) => {
    Controller.active(req, res, next);
});

app.patch('/user/inactive/:id', [verificaToken], (req, res, next) => {
    Controller.inactive(req, res, next);
});

app.post('/login', (req, res, next) => {
    Controller.login(req, res, next);
});

module.exports = app;