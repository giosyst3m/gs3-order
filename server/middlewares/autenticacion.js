const jwt = require('jsonwebtoken');
let access = require(`../models/access.model`);
const AccessControl = require('accesscontrol');

/**
 * Valid if token active or valid
 * @author Giosyst3m
 * @version 1.0.0
 * @param {string} token JWT
 * @returns boolean
 */
let validToken = (token, res) => {

    return jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            res.status(401).json({
                status: 401,
                result: false,
                err: { message: err.message }
            });
        } else {
            return true;
        }
    });



};

// =====================
// Verificar Token
// =====================
let verificaToken = (req, res, next) => {
    //console.log(req.query);
    let token = '';
    if (req.query.token) {
        token = req.query.token;
    } else {
        token = req.body.token;
    }


    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                status: 401,
                result: false,
                err: {
                    message: 'Token no válido'
                }
            });
        }

        req.user = decoded.user;
        next();

    });



};

// =====================
// Verifica AdminRole
// =====================
let verificaAdmin_Role = (req, res, next) => {

    let usuario = req.usuario;

    if (usuario.role === 'ADMIN_ROLE') {
        next();
    } else {

        return res.json({
            ok: false,
            err: {
                message: 'El usuario no es administrador'
            }
        });
    }
};

// =====================
// Verifica token para imagen
// =====================
let verificaTokenImg = (req, res, next) => {

    let token = req.query.token;

    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'Token no válido'
                }
            });
        }

        req.usuario = decoded.usuario;
        next();

    });


}

/**
 * Valid Acces fron user
 * @param {string} req params ad query { table, token }
 * @param {obj} res response
 * @param {obj} next response
 */
let validAccess = async(req, res, next) => {
    let table = '';
    if (req.params.table) {
        table = req.params.table;
    } else {
        table = req.query.table;
    }

    let user = await getUser(req.query.token);
    let grantList = await getAccess(user.role);
    if (grantList.length <= 0) {
        return res.status(403).json({
            status: 403,
            result: false,
            err: {
                message: 'Accesso Denegado'
            }
        });
    }

    const ac = new AccessControl(grantList);
    const acc = req.query.access || 'no-access';
    switch (acc) {
        case 'create:any':
            access_type = 'Crear';
            permission = ac.can(user.role).createAny(table);
            break;
        case 'create:own':
            access_type = 'Crear';
            permission = ac.can(user.role).crateAny(table);
            break;
        case 'read:any':
            access_type = 'Lectura';
            permission = ac.can(user.role).readAny(table);
            break;
        case 'read:own':
            access_type = 'Lectura';
            permission = ac.can(user.role).readOwn(table);
            break;
        case 'update:any':
            access_type = 'Actualizar';
            permission = ac.can(user.role).updateAny(table);
            break;
        case 'update:own':
            access_type = 'Actualizar';
            permission = ac.can(user.role).updateOwn(table);
            break;
        case 'delete:any':
            access_type = 'Eliminar';
            permission = ac.can(user.role).deleteAny(table);
            break;
        case 'delete:own':
            access_type = 'Eliminar';
            permission = ac.can(user.role).deleteOwn(table);
            break;
        default:
            console.log(`Acceso \x1b[31merror\x1b[0m User: ${user.name }, Perfil: \x1b[31m${ user.role }\x1b[0m, Acceso: \x1b[31m${ acc }\x1b[0m, Tabla: \x1b[41m\x1b[37m${ table }\x1b[0m\x1b[39m`);
            return res.status(403).json({
                status: 403,
                result: false,
                err: {
                    message: 'Acceso Denegado'
                }
            });
            break;
    }


    if (permission.granted) {
        //console.log('Acceso \x1b[32m%s\x1b[0m', 'ok', table, user.role, user.name);
        next();

    } else {
        // resource is forbidden for this user/role
        // return res.status(403).end();
        console.log(`Acceso \x1b[31merror\x1b[0m User: ${user.name }, Perfil: \x1b[31m${ user.role }\x1b[0m, Acceso: \x1b[31m${ acc }\x1b[0m, Tabla: \x1b[41m\x1b[37m${ table }\x1b[0m\x1b[39m`);
        return res.status(403).json({
            status: 403,
            result: false,
            err: {
                title: 'Acceso Denegado',
                message: `${user.name } no cuenta con permisos para ${ access_type} en "${ table }", favor contactar al administrador del sistema.`
            }
        });
    }



}

/**
 * Get Acces by Role
 * @param {string} role Name user role
 */
let getAccess = async(role) => {
    x = await access.find({ role, status: true }, { _id: 0, role: 1, resource: 1, action: 1, attributes: 1 });
    data = [];
    x.forEach(element => {
        data.push({
            role: element.role,
            resource: element.resource,
            action: element.action,
            attributes: element.attributes
        });
    });

    return data;
};

/**
 * Return Data from Token
 * @param {string} token Get Token JWT
 */
let getUser = async(token) => {

    data = await jwt.verify(token, process.env.SEED);
    return data.user;
};

module.exports = {
    validToken,
    verificaToken,
    verificaAdmin_Role,
    verificaTokenImg,
    validAccess
}