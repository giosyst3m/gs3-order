const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    document: {
        type: Schema.Types.ObjectId,
        ref: 'Document',
        required: true
    },
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    quantity: {
        type: Number,
        required: true,
        default: 1
    },
    request: {
        type: Number,
        required: false,
        default: 0,
    },
    inventory: {
        type: Boolean,
        required: true,
        default: false,
    },
    price: {
        type: Number,
        required: true
    },
    discount: {
        type: Schema.Types.ObjectId,
        ref: 'Discount',
    },
    origin: {
        type: String,
        required: true
    },
    note: {
        type: String,
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });
ModelSchema.index({ document: 1, product: 1 }, { unique: true });

module.exports = mongoose.model('Detail', ModelSchema);