let DocumentModel = require('../models/document.model');
let DetailModel = require('../models/detail.model');
let StatusModel = require('../models/state.model');
var { getMonthName } = require('../helpers/lib.helper');

module.exports = {

    getAllDocument: async(req, res, next) => {

        let type = req.params.type;
        let limit = Number(req.query.limit || 20);
        let skip = Number(req.query.skip || 0);
        let filters = {};
        if (req.query.number) {
            filters = {
                number: req.query.number
            }
        }
        if (req.query.status != '') {
            filters['status'] = {
                _id: req.query.status
            }
        }
        if (req.query.user != '') {
            filters['user'] = {
                _id: req.query.user
            }
        }
        dataDoc = [];
        if (req.user.role != '_ADMIN_' && req.user.role != '_GERENTE_' && req.user.role != '_BODEGA_') {
            filters.user = req.user._id;
        }
        DocumentModel.find(filters)
            .limit(limit)
            .skip(skip)
            .sort({ number: 'desc' })
            .populate({
                path: 'client tax discount delivery status type user',
                populate: {
                    path: 'city zone',
                    populate: {
                        path: 'state'
                    }
                }
            })
            .exec((err, docs) => {
                if (err) {
                    return res.status(500).json({
                        status: 500,
                        result: false,
                        err
                    });
                }
                DetailModel.aggregate([{
                    $group: {
                        _id: '$document',
                        total: { $sum: { $multiply: ['$quantity', '$price'] } },
                        count: { $sum: 1 }
                    }
                }])

                .exec((err, detail) => {
                    if (err) {
                        return res.status(500).json({
                            status: 500,
                            result: false,
                            err
                        });
                    }
                    for (const doc of docs) {
                        let total = 0;
                        let totalDis = 0;
                        let totalDel = 0;
                        let totaltax = 0;
                        for (const det of detail) {
                            if (String(det._id) === String(doc._id)) {
                                total = det.total;
                                if (doc.discount) {
                                    totalDis = ((total * doc.discount.discount) / 100);
                                    total = total - totalDis;
                                }

                                for (const iterator of doc.delivery) {
                                    totalDel = totalDel + iterator.price;
                                }

                                total = total + totalDel;

                                for (const iterator of doc.tax) {
                                    totaltax = totaltax + ((total * iterator.tax) / 100);
                                }

                                total = total + totaltax;

                                dataDoc.push({
                                    _id: doc._id,
                                    number: doc.number,
                                    doc: doc,
                                    PtotalDet: det.total,
                                    MtotalDis: totalDis,
                                    PtotalDel: totalDel,
                                    PtotalTax: totaltax,
                                    Total: total,
                                });
                            }
                        }
                    }
                    DocumentModel.count(filters, (err, total) => {
                        dataDocCompleate = [];
                        // ADD doucuments wich has been open and don't add detatil
                        for (const iterator of docs) {
                            const data = dataDoc.find(doc => doc.number === iterator.number);
                            if (!data) {
                                dataDocCompleate.push({
                                    _id: iterator._id,
                                    number: iterator.number,
                                    doc: iterator,
                                    PtotalDet: 0,
                                    MtotalDis: 0,
                                    PtotalDel: 0,
                                    PtotalTax: 0,
                                    Total: 0,
                                })
                            } else {
                                dataDocCompleate.push(data)
                            }
                        }
                        return res.status(200).json({
                            status: 200,
                            result: false,
                            data: dataDocCompleate,
                            total
                        });
                    });
                });

            });
    },
    getGroup: async(req, res, next) => {
        let type = req.params.group;
        let sta_or = Number(req.params.status);
        let limit = Number(req.query.limit || 12);
        let skip = Number(req.query.from || 0);
        let filters = {};
        let sta_filters = {};
        if (sta_or > 0) {
            sta_filters = { order: sta_or };
        }
        StatusModel.find(sta_filters)
            .select('_id')
            .exec((err, sta_resp) => {
                sta_id = [];
                for (const key in sta_resp) {
                    if (sta_resp.hasOwnProperty(key)) {
                        const element = sta_resp[key]['_id'];
                        sta_id.push(element);
                    }
                }

                data = [];
                _year = new Date().getFullYear();
                filters = {
                    status: { $in: sta_id },
                    date: { $gte: new Date(_year, 0, 1), $lte: new Date(_year, 11, 31) }
                }
                DocumentModel.find(filters)
                    .limit(limit)
                    .skip(skip)
                    .populate({
                        path: 'tax discount delivery status client',
                    })
                    .exec((err, docs) => {

                        DetailModel.aggregate([{
                            $group: {
                                _id: '$document',
                                total: { $sum: { $multiply: ['$quantity', '$price'] } },
                                count: { $sum: 1 }
                            }
                        }])

                        .exec(async(err, detail) => {

                            for (const doc of docs) {
                                let total = 0;
                                let totalDis = 0;
                                let totalDel = 0;
                                let totaltax = 0;
                                for (const det of detail) {
                                    if (String(det._id) == String(doc._id)) {
                                        total = det.total;
                                        if (doc.discount) {
                                            totalDis = ((total * doc.discount.discount) / 100);
                                            total = total - totalDis;
                                        }

                                        for (const iterator of doc.delivery) {
                                            totalDel = totalDel + iterator.price;
                                        }

                                        total = total + totalDel;

                                        for (const iterator of doc.tax) {
                                            totaltax = totaltax + ((total * iterator.tax) / 100);
                                        }

                                        total = total + totaltax;
                                        data.push({
                                            _id: doc._id,
                                            status_id: doc.status._id,
                                            status: doc.status.name,
                                            status_color: doc.status.color,
                                            status_icon: doc.status.icon,
                                            status_order: doc.status.order,
                                            number: doc.number,
                                            PtotalDet: det.total,
                                            MtotalDis: totalDis,
                                            PtotalDel: totalDel,
                                            PtotalTax: totaltax,
                                            Total: total,
                                            date: doc.date,
                                            month: new Date(doc.date).getMonth() + 1,
                                            year: new Date(doc.date).getFullYear(),
                                            monthName: await getMonthName(doc.date)
                                        });
                                    }
                                }
                            }
                            var result = [];
                            switch (type) {
                                case 'status':
                                    data.reduce(function(res, value) {
                                        if (!res[value.status]) {
                                            res[value.status] = {
                                                total: 0,
                                                count: 0,
                                                name: value.status,
                                                color: value.status_color,
                                                icon: value.status_icon,
                                                order: value.status_order
                                            };
                                            result.push(res[value.status]);
                                        }
                                        res[value.status].total += value.Total;
                                        res[value.status].count++;
                                        return res;
                                    }, {});
                                    return res.status(200).json({
                                        status: 200,
                                        result: false,
                                        data: result
                                    });
                                    break;
                                case 'month':
                                    data.reduce(function(res, value) {
                                        if (!res[value.month]) {
                                            res[value.month] = {
                                                total: 0,
                                                count: 0,
                                                month: value.month,
                                                monthName: value.monthName
                                            };
                                            result.push(res[value.month]);
                                        }
                                        res[value.month].total += value.Total;
                                        res[value.month].count++;
                                        return res;
                                    }, {});
                                    total = [];
                                    for (const key in result) {
                                        if (result.hasOwnProperty(key)) {
                                            const element = result[key]['total'];
                                            total.push(element);
                                        }
                                    }
                                    month = [];
                                    for (const key in result) {
                                        if (result.hasOwnProperty(key)) {
                                            const element = result[key]['month'];
                                            month.push(element);
                                        }
                                    }
                                    monthName = [];
                                    for (const key in result) {
                                        if (result.hasOwnProperty(key)) {
                                            const element = result[key]['monthName'];
                                            monthName.push(element);
                                        }
                                    }
                                    count = [];
                                    for (const key in result) {
                                        if (result.hasOwnProperty(key)) {
                                            const element = result[key]['count'];
                                            count.push(element);
                                        }
                                    }
                                    return res.status(200).json({
                                        status: 200,
                                        result: false,
                                        data: result,
                                        total,
                                        month,
                                        monthName,
                                        count
                                    });
                                    break;
                                case 'year':
                                    data.reduce(function(res, value) {
                                        if (!res[value.year]) {
                                            res[value.year] = {
                                                total: 0,
                                                count: 0,
                                                year: value.year,
                                            };
                                            result.push(res[value.year]);
                                        }
                                        res[value.year].total += value.Total;
                                        res[value.year].count++;
                                        return res;
                                    }, {});
                                    break;

                                default:
                                    resutl = data;
                                    break;
                            }
                        });

                    });
            });
    }
};