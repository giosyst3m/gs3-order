const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    number: {
        type: String,
        required: [true, 'Numero de Factura requerido']
    },
    price: {
        type: Number,
        required: [true, 'Monto es Requerido'],
    },
    status: {
        type: Boolean,
        required: [true, 'Debe indicar el Estado'],
        default: true
    },
    document: {
        type: Schema.Types.ObjectId,
        ref: 'Document',
        required: [true, 'Debe estar asociado']
    },
    date: {
        type: Date,
        required: false
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('DocumentRelate', ModelSchema);