const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/client.controller`);
const app = express();

app.get('/client', [verificaToken, validAccess], (req, res, next) => {
    Controller.getAll(req, res, next);
});
app.get('/client/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getById(req, res, next);
});

module.exports = app;