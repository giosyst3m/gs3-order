const Model = require('../models/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mdAuthentication = require('../middlewares/autenticacion');

const { sendEmailUserCreate, sendEmailReseatPassword } = require('../helpers/emails.helper');

module.exports = {
    /**
     * Get all user
     * @author Giosyst3m
     * @version 1.0.0
     * @returns all user
     */
    index: async(req, res, next) => {
        Model.find({}, '_id name lastname email img')
            .exec((error, data) => {
                if (error) {
                    return res.status(500).json({
                        status: 500,
                        result: false,
                        error
                    });
                }

                return res.status(200).json({
                    status: 200,
                    result: true,
                    data
                });
            });
    },
    /**
     * Create a user
     * @author Giosyst3m
     * @param body Array User Data
     * @returns get back user create
     */
    create: async(req, res, next) => {
        try {
            password = req.body.name + Math.floor(Math.random() * 9999);
            req.body.password = bcrypt.hashSync(password, 10);
            req.body.user = req.user._id;
            const newRecord = new Model(req.body);
            const record = await newRecord.save();
            record.password = ':-)';
            await sendEmailUserCreate(req);

            res.status(200).json({
                status: 200,
                result: true,
                data: record,
                user: req.user
            });


        } catch (err) {
            res.status(400).json({
                status: 400,
                result: false,
                err: err
            });
        }
    },
    /**
     * Update a user from ID
     */
    update: async(req, res, next) => {
        try {
            const { id } = req.params;
            const newRecord = req.body;
            const oldRecord = await Model.findByIdAndUpdate(id, newRecord);
            oldRecord.password = ':-)';
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    active: async(req, res, next) => {
        try {
            const { id } = req.params;
            const oldRecord = await Model.findByIdAndUpdate(id, { status: true }, { new: true, runValidators: true });
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },

    inactive: async(req, res, next) => {
        try {
            const { id } = req.params;
            const oldRecord = await Model.findByIdAndUpdate(id, { status: false }, { new: true, runValidators: true });
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },

    getById: async(req, res, next) => {
        try {
            const { id } = req.params;
            const data = await Model.findById(id);
            if (!Model) {
                res.status(400).json({
                    status: 400,
                    result: false,
                    err: {
                        message: 'ID no existe'
                    }
                });
            } else {
                res.status(200).json({
                    status: 200,
                    result: true,
                    data: data
                });
            }
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }

    },
    login: async(req, res, next) => {
        try {
            var body = req.body;
            const data = await Model.findOne({ email: body.email })
            if (!data) {
                res.status(400).json({
                    status: 400,
                    result: false,
                    err: {
                        message: 'Datos errados'
                    }
                });
            } else {
                if (bcrypt.compareSync(body.password, data.password)) {
                    var user = {
                        email: data.email,
                        name: data.name + ' ' + data.lastname,
                        role: data.role,
                        _id: data._id,
                        avatar: data.avatar, 
                        client: data.client,
                    };
                    var token = jwt.sign({ user: user }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });
                    user['token'] = token;
                    res.status(200).json({
                        status: 200,
                        result: true,
                        data: user
                    });
                } else {
                    res.status(400).json({
                        status: 400,
                        result: true,
                        error: { message: 'Datos errados' }
                    });
                }

            }
        } catch (err) {

            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }

    },
    /**
     * Change user password by ID
     * @param id string User ID
     * @param password string Nuew password.
     * @return { result, status, [email] [err]}
     */
    resaetPassword: async(req, res, next) => {
        try {
            const { id, password } = req.body;
            newPass = {
                password: bcrypt.hashSync(password.password, 10),
            }
            const oldRecord = await Model.findByIdAndUpdate(id, newPass);
            oldRecord.password = ':-)';
            await sendEmailReseatPassword(oldRecord, password.password);
            res.status(200).json({
                status: 200,
                result: true,
                data: { email: oldRecord.email }
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    /**
     * Change user password by ID
     * @param email string User email
     * @param password string Nuew password.
     * @return { result, status, [email] [err]}
     */
    recoveryPassword: async(req, res, next) => {
        try {
            const { email } = req.body;
            password = email.split('@')[0] + Math.floor(Math.random() * 9999);
            newPass = {
                password: bcrypt.hashSync(password, 10),
            }
            Model.findOneAndUpdate({ email: email }, newPass, async(err, resp) => {
                if (err) {
                    res.status(500).json({
                        status: 500,
                        result: true,
                        error: err,
                    });
                }
                if (!resp) {
                    res.status(404).json({
                        status: 404,
                        result: true,
                        error: {
                            message: `El email <b>${ email }</b> no existe`
                        }
                    });
                } else {
                    resp.password = ':-)';
                    await sendEmailReseatPassword(resp, password, 'Recuperación');
                    res.status(200).json({
                        status: 200,
                        result: true,
                        data: { email: resp.email }
                    });
                }
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    }
}