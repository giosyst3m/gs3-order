const DocumentModel = require('../models/document.model');
let DeliveryModel = require(`../models/delivery.model`);

module.exports = {
    /**
     * @param model of Delivery
     * @param from number from record wants to start
     * @param value string value to search
     * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
     * @param status number Get Records 1 = Active 2 = Inactive 3 = both
     * @param body array data to save
     * @return object {Status, Result, Data, TotalRecords, [error]}
     */
    save: async(req, res, next) => {
        try {
            let body = req.body;
            body['user'] = req.user._id;
            let Model = new DeliveryModel(body);


            Model.save((err, del) => {
                if (err) {
                    return res.status(400).json({
                        status: 400,
                        result: false,
                        err
                    });
                }

                DocumentModel.findByIdAndUpdate(req.params.document, { delivery: [del._id] }, { new: true, runValidators: true }, (err, doc) => {

                    if (err) {
                        return res.status(400).json({
                            status: 400,
                            result: false,
                            err
                        });
                    }
                    res.json({
                        status: 200,
                        result: true,
                        delivery: del,
                    });
                });


            });

        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },
}