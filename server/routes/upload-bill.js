const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const nodemailer = require('nodemailer');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const fs = require('fs');
const path = require('path');
var XLSX = require('xlsx');
var cloudinary = require('cloudinary');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

const DocumentRelateModel = require('../models/documentrelate.model');
const DocumentModel = require('../models/document.model');
const StatusModel = require('../models/status.model');

app.put('/upload/import/bill/:code', [verificaToken, validAccess, multipartMiddleware], async(req, res, next) => {
    console.log('START: \x1b[36mBill\x1b[0m  - Update: Document: Order');
    x = Date();
    if (!req.files) {
        return res.status(400)
            .json('No se ha seleccionado ningún archivo');
    }
    // Obtener el nombre del archivo
    var file = req.files.file;
    var shortName = file.name.split('.');
    var ext = shortName[shortName.length - 1];

    // Valid extention
    var extensions = ['xls', 'xlsx'];

    if (extensions.indexOf(ext) < 0) {
        return res.status(400)
            .json(
                `La extensión del archivo es ${ ext } no es permitido, solo debe subir con extenciones ` + extensions.join(', ')
            );
    }

    // Upload Cloudinary
    _cloudinary = await cloudinary.uploader.upload(file.path, function(result) {
        return result;
    }, { resource_type: "raw", folder: 'import/bill', tags: ['import ', 'bill'] });

    // Get Document and bill update
    data = await getBills(file);

    // Send email with resoult
    sendMail(data, req.user);
    console.log('END: \x1b[36mBill\x1b[0m - Update: Document: Order:  \x1b[37m\x1b[41m%s\x1b[0m\x1b[40m ', data.length);
    console.log(x);
    console.log(Date());
    return res.status(200)
        .json({
            ok: true,
            message: 'Arhivos subidos',
            cloudinary: { secure_url: _cloudinary.secure_url },
            data
        });
});

/**
 * Search in excel fil all bills numbers
 * @param {file} file 
 * @returns bills[] with Documents updated
 */
async function getBills(file) {
    // Open Excel File
    var buf = fs.readFileSync(file.path);
    var wb = XLSX.read(buf, { type: 'buffer' });

    const table = XLSX.readFile(file.path);
    const sheet = table.Sheets[table.SheetNames[0]];
    var range = XLSX.utils.decode_range(sheet['!ref']);
    bills = [];
    data = [];
    // Get ID Status Pagado
    status = await StatusModel.findOne({ order: 6 });
    // Get ID Status Cobrar
    _status = await StatusModel.findOne({ order: 5 });
    // For to check real information come from excel file
    for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
        if (sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })]) {
            // Get Bill Number
            bill = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.match(/\-(.*?)\-/);
            // Check if there is a number bill valid
            if (bill) {
                // Get Date form Bill
                date = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim().substr(sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim().length - 10);
                // Conver Bill in Number
                bill = Number(bill[1].trim());
                // Get all document with number Bill
                DocRelate = await DocumentRelateModel.find({ number: bill });
                doc = [];
                // For to update all document with Bill number
                for (let index = 0; index < DocRelate.length; index++) {
                    const element = DocRelate[index];
                    // Update Bill to Close and Date pay
                    await DocumentRelateModel.findByIdAndUpdate(element._id, { status: false, date: date });
                    // Update document relate to bill
                    _doc = await DocumentModel.findOneAndUpdate({ _id: element.document }, { status: status._id, pay_date: date, pay_total: element.price });
                    if (_doc) {
                        doc.push({
                            bill: element.number,
                            doc: _doc.number,
                            status: status.name
                        });
                    }
                }
                data.push({
                    status: DocRelate.length > 0 ? 200 : 404,
                    result: DocRelate.length > 0 ? true : false,
                    bill: bill,
                    date: date,
                    doc: doc
                })
            }
        }
    }
    return data;
}

function sendMail(data, user) {
    html = '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr style="color: #fff;background-color: #337ab7;"><td>#</td><td>Actualizado</td><td>Factura</td><td>Fecha de Pago</td><td>Pedidos</td></tr></thead>';
    html += '<tbody>';
    i = 1;
    for (let index = 0; index < data.length; index++) {
        const element = data[index];
        html += '<tr>';
        html += `<td>${ i }</td>`;
        html += `<td>${ element.result?'Si':'No' }</td>`;
        html += `<td>${ element.bill }</td>`;
        html += `<td>${ element.date }</td>`;
        html += `<td><ul>`;
        for (let index2 = 0; index2 < element.doc.length; index2++) {
            const element2 = element.doc[index2];
            html += `<li>${ element2.doc } - ${ element2.status }</li>`
        }
        html += `</ul></td>`;
        html += '</tr>';
        i++;
    }
    html += '</tbody></table>';
    //Send Email to notify Beging process Excel File
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: process.env.mailHost,
            port: process.env.mailPort,
            secure: process.env.mailSecure, // true for 465, false for other ports
            auth: {
                user: process.env.mailUser, // generated ethereal user
                pass: process.env.mailPass // generated ethereal password
            }
        });
        // setup email data with unicode symbols
        let mailOptions = {
            from: process.env.mailFrom, // sender address
            to: user.email || process.env.mailFrom, // list of receivers
            subject: 'Importación de Facturas para Pagos de Pedidos Finalizada', // Subject line
            text: 'Se inicia el Procesos de Productos y actualización de inventario, ', // plain text body
            html: '<b>Peodidos OnLine</b><p>Se Finalizó el Procesos de Actualizar Pedidos por Facturas pagadas, favor hacer la revisión en el sistema </p>' + html // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    });
}
module.exports = app;