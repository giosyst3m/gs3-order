const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/log.controller`);
const app = express();

app.get('/log/:entity/:id', [verificaToken, validAccess], (req, res, next) => {
    Controller.getByEntityId(req, res, next);
});


module.exports = app;