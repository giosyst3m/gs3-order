const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;


let deliverytSchema = new Schema({
    company: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    status: {
        type: Boolean,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});


module.exports = mongoose.model('Delivery', deliverytSchema);