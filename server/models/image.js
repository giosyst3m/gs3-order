const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = mongoose.model('Imagen', new Schema({
    file: {
        type: String,
        required: true
    },
    table: {
        type: Schema.Types.ObjectId,
        ref: 'Table',
        required: true
    },
    row: {
        type: Number,
        required: true
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },


}));