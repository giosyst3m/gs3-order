const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var role = {
    values: ['_GERENCIA_', '_BODEGA_', '_VENTAS_', '_CLIENTE_'],
    message: 'No es un ROL valido'
}

let ModelSchema = new Schema({
    name: {
        type: String,
        required: [true, 'El Nombre es requerido']
    },
    lastname: {
        type: String,
        required: [true, 'El apellido es requerido']
    },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        unique: true
    },
    password: {
        type: String,
        required: [false, 'La contraseña es requerida']
    },
    role: {
        type: String,
        default: '_VENTAS_',
        enum: role,
        required: true
    },
    avatar: {
        type: String,
        required: false,
        default: 'avatar'
    },
    client: {
        type: String,
        required: false
    },
    company: [{
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: [true, 'La Empresa es requerida']
    }],
    zone: [{
        type: Schema.Types.ObjectId,
        ref: 'Zone',
        required: [true, 'La Zona es requerida']
    }],
    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});
ModelSchema.index({ email: 1, }, { unique: true });
ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('User', ModelSchema);