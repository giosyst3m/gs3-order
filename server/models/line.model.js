const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let LineSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    code: {
        type: Number,
        required: true
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
});

LineSchema.index({ code: 1 }, { unique: true });
LineSchema.index({ name: 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Line', LineSchema);