const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = mongoose.model('Condition', new Schema({
    name: {
        type: String,
        required: false
    },
    code: {
        type: Number,
        required: true,
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },


}));