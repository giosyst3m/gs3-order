const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/dashboard.controller`);

let app = express();

// ============================
// Get all Document Resum
// ============================
app.get('/dashboard/document/:type', [verificaToken, validAccess], (req, res, next) => {
    Controller.getAllDocument(req, res, next);
});

// ============================
// Get all Rows Group By {GRUOP}
// ============================
app.get('/dashboard/group/:group/:status', [verificaToken, validAccess], (req, res, next) => {
    Controller.getGroup(req, res, next);
});


module.exports = app;