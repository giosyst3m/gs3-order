const ZonaModel = require('../models/zone.model');
const UserModel = require('../models/user.model');
const Model = require(`../models/log.model`);
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {

    getByEntityId: async(req, res, next) => {
        let limit = Number(req.query.limit || 5);
        let skip = Number(req.query.from || 0);
        filters = { entity: req.params.entity, id: req.params.id };
        data = await Model.find(filters)
            .populate('user')
            .sort({ date: 'desc' })
            .limit(limit)
            .skip(skip);
        res.status(200).json({
            status: 200,
            result: true,
            total: await Model.count(filters),
            data
        });
    },
}