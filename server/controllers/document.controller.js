const Model = require('../models/document.model');
const CounterModel = require('../models/counter.model');
const StatusModel = require('../models/status.model');
const ClientModel = require('../models/client.model');
const DetailModel = require('../models/detail.model');
const ProductModel = require('../models/product.model');
const UserModel = require('../models/user.model');
const RoleModel = require('../models/role.model');
const DocumentRelateModel = require('../models/documentrelate.model');
require('../models/discount.model');
require('../models/delivery.model');
var fs = require('fs');
var pdf = require('html-pdf');
var cloudinary = require('cloudinary');
var { getPDFHeader, getPDFooter } = require('../helpers/pdf.helper');
var { sendEmailDocPDF } = require('../helpers/emails.helper');
var { deleteFiles, saveLog } = require('../helpers/lib.helper');


module.exports = {
    /**
     * Search Document by Client, Type and Status 
     * @author giosyst3m
     */
    getDocByClient: async(req, res, next) => {
        try {
            // Get ID by Conditional
            let counter = await CounterModel.findOne({ name: req.params.type, company: req.params.company });
            let status = await StatusModel.findOne({ order: req.params.status });
            let client = await ClientModel.findById(req.params.client);


            let _document = await Model.findOne({
                    client: { _id: req.params.client },
                    counter: { _id: counter._id },
                    status: { _id: status._id },
                    company: { _id: req.params.company }
                })
                .populate('status')
                .populate('type')
                .populate('client');

            if (!_document) {
                return res.status(404).json({
                    status: 404,
                    result: false,
                    message: `No se encontro Pedido con estatus "${ status.name }" para el cliente ${ client.name }`

                });
            }

            return res.json({
                status: 200,
                result: true,
                data: _document
            });
        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },

    createHeader: async(req, res, next) => {
        // Get ID by Conditional
        let counter = await CounterModel.findOne({ name: req.params.type, company: req.body.company });
        if (!counter) {
            res.status(404).json({
                status: 404,
                result: false,
                err: {
                    message: 'Favor validar el contador de la empresa'
                }
            });
        }
        req.body['number'] = counter.number + 1;
        let _counter = await CounterModel.findByIdAndUpdate({ _id: counter._id }, { number: req.body.number });
        let status = await StatusModel.findOne({ order: req.params.status });
        req.body['status'] = status._id;
        req.body['counter'] = counter._id;
        req.body['user'] = req.user._id;
        let Data = new Model(req.body);
        _data = await Data.save();
        res.json({
            status: 200,
            result: true,
            data: _data
        });
    },

    /**
     * Create detail/rows in a Document
     */
    createDetail: async(req, res, next) => {
        let product = await ProductModel.findById(req.body.product);
        req.body['price'] = product.price;
        req.body['user'] = req.user._id;
        let Model = new DetailModel(req.body);
        Model.save((err, data) => {
            if (err) {
                return res.status(400).json({
                    status: 400,
                    result: false,
                    err
                });
            }
            res.json({
                status: 200,
                result: true,
                data
            });
        });


    },

    /**
     * Get all detail by Document ID
     * @author giosyst3m
     */
    getDetail: async(req, res, next) => {
        let limit = Number(req.query.limit || 10);
        let skip = Number(req.query.from || 0);
        _data = await DetailModel.find({ document: { _id: req.params.id }, status: true })
            .sort({ date: 'desc' })
            .limit(limit)
            .skip(skip)
            .populate({
                path: 'product document',
                populate: {
                    path: 'brand category type line conditional'
                },
                options: {
                    sort: {
                        barcode: 'descending'
                    }
                }
            });

        res.json({
            status: 200,
            result: true,
            data: _data,
            total: await DetailModel.count({ document: { _id: req.params.id }, status: true })
        });

    },
    /**
     * Get Document by ID
     * @author giosyst3m
     */
    getById: async(req, res, next) => {
        Model.findOne({ _id: req.params.id })
            .populate('user', 'name lastname avatar')
            .populate({
                path: 'client status tax type discount delivery',
                populate: {
                    path: 'category city zone buttom role addProduct',
                    populate: {
                        path: 'state'
                    }

                }
            })
            .exec((err, data) => {
                if (err) {
                    return res.status(500).json({
                        status: 4002,
                        result: false,
                        err
                    });
                }

                res.json({
                    status: 200,
                    result: true,
                    data
                });

            })

    },
    /**
     * Get Document by ID
     * @author giosyst3m
     */
    getTotal: async(req, res, next) => {
        DetailModel.find({ document: req.params.id })
            .populate({
                path: 'product',
                populate: {
                    path: 'brand line type condition'
                },
            })
            .exec((errors, data) => {
                if (errors) {
                    return res.status(500).json({
                        status: 500,
                        result: false,
                        errors
                    });
                }
                let qty = 0;
                let sum = 0;

                for (const iterator of data) {
                    qty = qty + iterator.quantity;
                    sum = sum + (iterator.quantity * iterator.price);
                }

                Model.findOne({ _id: req.params.id })
                    .populate('tax')
                    .populate('discount')
                    .populate('delivery')
                    .exec((err, doc) => {
                        let discount = 0;
                        let total = 0;
                        let delivery = 0;
                        let tax = 0;
                        if (err) {
                            return res.status(500).json({
                                status: 500,
                                result: false,
                                errrs
                            });
                        }

                        if (doc.discount) {
                            discount = (doc.discount.discount / 100) * sum;
                        }

                        total = sum - discount;

                        for (const iterator of doc.delivery) {
                            delivery = delivery + iterator.price;
                        }

                        total = total + delivery;

                        for (const iterator of doc.tax) {
                            tax = tax + ((iterator.tax / 100) * total);
                        }

                        total = total + tax;

                        res.json({
                            status: 200,
                            result: true,
                            data: {
                                quantity: qty,
                                sum: sum,
                                tax: tax,
                                delivery: delivery,
                                discount: discount,
                                total: total
                            },

                        });
                    });

            });
    },
    /**
     * Add Documente Relate
     * @author giosyst3m
     */
    addDocRelate: async(req, res, next) => {
        let body = req.body;
        body['document'] = req.params.id;
        body['user'] = req.user._id;
        let Model = new DocumentRelateModel(body);

        Model.save((errors, data) => {
            if (errors) {
                return res.status(400).json({
                    status: 400,
                    result: false,
                    errors
                });
            }

            res.json({
                status: 200,
                result: true,
                data
            });


        });
    },
    /**
     * Add Documente Relate
     * @author giosyst3m
     */
    getDocRelateById: async(req, res, next) => {
        let limit = Number(req.query.limit || 5);
        let skip = Number(req.query.from || 0);
        _doc = await DocumentRelateModel.find({ document: req.params.id })
            .limit(limit)
            .skip(skip);
        res.json({
            status: 200,
            result: true,
            data: _doc,
            total: await DocumentRelateModel.count({ document: req.params.id })
        });
    },
    deleteDetailInZero: async(req, res, next) => {
        data = await DetailModel.deleteMany({ document: req.params.id, inventory: false });
        res.json({
            status: 200,
            result: true
        });
    },
    /**
     * Add Documente Relate
     * @author giosyst3m
     */
    getByNumber: async(req, res, next) => {
        Model.findOne({ number: req.params.number }, (err, resp) => {
            if (err) {
                res.json({
                    status: 200,
                    result: true,
                    err
                });
            }
            res.json({
                status: 200,
                result: true,
                data: resp
            });
        }).populate('status');

    },
    getPDFByID: async(req, res, next) => {
        let _img = req.body.img;
        html = await getPDFHeader(req.body.company);

        console.log(`START: PDF: Document: \x1b[36m${req.params.id}\x1b[0m`);
        _doc = await Model.findOne({ _id: req.params.id })
            .populate('user', 'name lastname avatar')
            .populate({
                path: 'client status tax type discount delivery',
                populate: {
                    path: 'category city zone buttom role',
                    populate: {
                        path: 'state'
                    }

                }
            });

        _footer = await getPDFooter(`Pedido Nro: ${ _doc.number.toLocaleString('en',{
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
            }) }`);
        _data = await DetailModel.find({ document: { _id: req.params.id }, status: true })
            .sort({ date: 'desc' })
            .populate({
                path: 'product document',
                populate: {
                    path: 'brand category type line conditional'
                },
                options: {
                    sort: {
                        barcode: 'descending'
                    }
                }
            });

        DetailModel.find({ document: req.params.id })
            .populate({
                path: 'product',
                populate: {
                    path: 'brand line type condition'
                },
            })
            .exec((errors, data) => {
                if (errors) {
                    return {
                        status: 500,
                        result: false,
                        errors
                    };
                }
                let qty = 0;
                let sum = 0;

                for (const iterator of data) {
                    qty = qty + iterator.quantity;
                    sum = sum + (iterator.quantity * iterator.price);
                }

                Model.findOne({ _id: req.params.id })
                    .populate('tax')
                    .populate('discount')
                    .populate('delivery')
                    .exec((err, doc) => {
                        let discount = 0;
                        let total = 0;
                        let delivery = 0;
                        let tax = 0;
                        if (err) {
                            return {
                                status: 500,
                                result: false,
                                err
                            };
                        }

                        if (doc.discount) {
                            discount = (doc.discount.discount / 100) * sum;
                        }

                        total = sum - discount;
                        _delivery = '';
                        for (const iterator of doc.delivery) {
                            delivery = delivery + iterator.price;
                            _delivery += `<tr>
                                        <td width="70%">&nbsp;</td>
                                        <td width="25%" class="total"><b>${ iterator.company }</b></td>
                                        <td width="15%" class="right total" >${ iterator.price.toLocaleString('en', {   
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        }) }</td>
                                    </tr>`;
                        }

                        total = total + delivery;
                        _tax = '';
                        for (const iterator of doc.tax) {
                            tax = tax + ((iterator.tax / 100) * total);
                            _tax += `<tr>
                                        <td width="70%">&nbsp;</td>
                                        <td width="25%" class="total"><b>${ iterator.name }</b> <b class="label dark">${ iterator.tax }%</b></td>
                                        <td width="15%" class="right total" >${ tax.toLocaleString('en', {   
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        }) }</td>
                                    </tr>`;

                        }

                        total = total + tax;
                        var a = new Date(_doc.date);
                        html += `
        <html>
            <head>
                <style>
                body {
                        color: #73879C;
                        font-family: "Helvetica Neue",Roboto,Arial,"Droid Sans",sans-serif;
                        font-size: 10px !important;
                        font-weight: 400;
                        line-height: 1.471;
                    }
                    baged {
                        display: inline-block;
                        min-width: 10px;
                        padding: 3px 7px;
                        font-size: 9px;
                        font-weight: 700;
                        line-height: 1;
                        color: #fff;
                        text-align: center;
                        white-space: nowrap;
                        vertical-align: middle;
                        background-color: #777;
                        border-radius: 10px;
                    }
                    .bg-blue, th {
                        background: #3498DB!important;
                        border: 1px solid #3498DB!important;
                        color: #fff;
                    }
                    .label-primary {
                        background-color: #337ab7;
                    }
                    .label-warning {
                        background-color: #f0ad4e;
                    }
                    .label {
                        display: inline;
                        padding: .2em .6em .3em;
                        font-size: 75%;
                        font-weight: 700;
                        line-height: 1;
                        color: #fff;
                        text-align: center;
                        white-space: nowrap;
                        vertical-align: baseline;
                        border-radius: .25em;
                    }
                    th, tr.border td {
                        font-size: 10px !important;
                        padding: 3px;
                    }
                    .par {
                        background-color: #f9f9f9;
                        font-size: 10px !important;
                    }
                    .lead {
                        font-size: 21px;
                    }
                    .center {
                        text-align: center
                    }
                    .right {
                        text-align: right
                    }
                    .dark {
                        color: #E9EDEF;
                        background-color: #4B5F71;
                        border-color: #364B5F;
                    }
                    .text-danger {
                        color: #a94442;
                    }
                    .total {
                        padding: 8px;
                        line-height: 1.42857143;
                        vertical-align: top;
                        border-top: 1px solid #ddd
                    }
                    .table-bordered {
                        border: 1px solid #ddd;
                    }
                    .table {
                        width: 100%;
                        max-width: 100%;
                        margin-bottom: 20px;
                        border-spacing: 0;
                        border-collapse: collapse;
                        font-size: 10px !important;
                    }
                    thead {
                        display: table-header-group;
                        vertical-align: middle;
                        border-color: inherit;
                    }
                    tr {
                        display: table-row;
                        vertical-align: inherit;
                        border-color: inherit;
                    }
                    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
                        border: solid #ddd;
                        border-width: thin;
                    }
                    
                </style>
            </head>
            <body>
                <h2 stye>Pedido Nro: ${ _doc.number }</h2>
                <table style="width:100%; font-size: 10px !important;" cellspacing="0">
                    <head>
                        <tr>
                            <td colspan="5">${ _doc.client.name }</td>
                            <td colspan="4">Estado: ${ _doc.status.name}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <b>NIT ${ _doc.client.code } |</b> <span class="baged bg-blue">${ _doc.client.correlative }</span><br>
                                ${ _doc.client.address }<br>
                                <span class="label label-primary">${ _doc.client.city.name}, ${ _doc.client.city.state.name }</span>
                                <span class="label label-warning">${ _doc.client.zone.name }</span>
                            </td>
                            <td colspan="5">
                                <b>Datos Creación</b><br>
                                ${ _doc.user.name } ${ _doc.user.lastname }<br>
                                <b>Fecha:</b> ${ a.getDay() }/${ a.getMonth() }/${ a.getFullYear() }<br>
                                <b>Hora:</b> ${ a.getHours() }:${ a.getMinutes() }:${ a.getMilliseconds() }
                            </td>
                        </tr>
                    </thaed>
                </table>
                <br>
                <table style="width:100%" cellspacing="0" class="table-bordered">
                    <thaed>
                        <tr>
                            <th>#</th>`;
                        if (_img) {
                            html += `<th>IMG</th>`;
                            html += `<th>Descripción</th>`;
                        } else {
                            html += `<th>Descripción</th>`;
                            html += `<th>Barcode</th>`;
                            html += `<th>REF</th>`;
                        }
                        html += `
                            <th>Marca</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Total</th>
                        </tr>
                    </thead><tbody>`;

                        for (let index = 0; index < _data.length; index++) {
                            const element = _data[index];
                            clase = (index % 2) ? 'impar' : 'par';
                            html += `
                    <tr class="border ${ clase  }" >
                        <td class="right">${ index + 1 }</td>`;
                            if (_img) {
                                html += `<td class="center"> ${ cloudinary.image(element.product.img, { width: 40, crop: "scale" })}</td>`;
                                html += `<td>${ element.product.name }
                                        <br><b>Barcode:</b> ${ element.product.barcode }
                                        <br><b>REF:</b> ${ element.product.sku }</td>`;
                            } else {
                                html += `<td>${ element.product.name }</td>`;
                                html += `<td>${ element.product.barcode }</td>`;
                                html += `<td>${ element.product.sku }</td>`;
                            }

                            html += `<td class="center">${ element.product.brand.name }</td>`;
                            html += `<td class="right">${ element.price.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) }</td>
                        <td class="right">${ element.quantity.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) }</td>
                        <td class="right">${ (element.price * element.quantity).toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) }</td>
                    </tr>
                `;
                        }

                        html += `</tbody>
                        </table>
                                <table style="width:100%; font-size: 10px !important;">
                                    <tr>
                                        <td width="60%">&nbsp;</td>
                                        <td width="40%" class="lead">Total</td>
                                    </tr>
                                    <tr>
                                        <td width="60%">&nbsp;</td>
                                        <td width="25%" class="total" ><b>Subtotal</b></td>
                                        <td width="15%" class="right total">${ sum.toLocaleString('en', {   
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        }) }</td>
                                    </tr>`;

                        if (_doc.discount != undefined) {
                            html += ` 
                                    <tr>
                                        <td width="70%">&nbsp;</td>
                                        <td width="25%" class="total" ><b>Descuento</b> <b class="label dark">${ _doc.discount.name }</b></td>
                                        <td width="15%"class="right total text-danger">-${ discount.toLocaleString('en', {   
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        }) }</td>
                                    </tr>`;
                        }

                        html += `${ _delivery } 
                                ${ _tax }
                                    <tr>
                                        <td width="70%">&nbsp;</td>
                                        <td width="25%" class="total" ><b>Total</b></td>
                                        <td class="total" width="15%"class="right" ><b>${ total.toLocaleString('en', {   
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        }) }</b></td>
                                    </tr>
                                </table>
                                `;
                        html +=
                            `
            </body>
        </html>
        `;
                        var currentTime = new Date();
                        var options = {
                            format: 'Letter',
                            paginationOffset: 1, // Override the initial pagination number
                            'header': {
                                'height': '70px',
                            },
                            'footer': {
                                'height': '30px',
                                "contents": {
                                    default: _footer
                                }

                            }
                        };
                        _file = `./public/pdf/${ req.params.doc }-${ _doc.number }.pdf`;
                        pdf.create(html, options).toFile(_file, async function(err, file) {
                            // Upload Cloudinary
                            cloudinary.uploader.upload(file.filename, async(_cloudinary) => {
                                if (req.body.email != undefined) {
                                    await sendEmailDocPDF(req.user, req.body.email, _cloudinary.secure_url, `Pedido Nro.: ${ _doc.number }`)
                                }
                                await deleteFiles(_file);
                                console.log(`FINISH: PDF: Document: \x1b[36m${req.params.id}\x1b[0m`);
                                res.json({
                                    status: 200,
                                    result: true,
                                    data: {
                                        file: {
                                            url: _cloudinary.url,
                                            secure_url: _cloudinary.secure_url,
                                            public_id: _cloudinary.public_id
                                        }
                                    },
                                });
                            }, { resource_type: "raw", folder: `export/${ req.params.doc }`, public_id: `${ req.params.doc }-${_doc.number }`, tags: ['export ', req.params.doc, 'pdf', _doc.number] });
                        });
                    });

            });

    },
    updateById: async(req, res, next) => {
        try {
            const { total } = req.body;
            const { id } = req.params;
            const mongoose = require('mongoose');
            const ObjectId = mongoose.Types.ObjectId;

            status = await StatusModel.findById(req.body.status);
            if (status.validBill) {
                detail = await DetailModel.find({
                    document: ObjectId(id),
                    inventory: false
                },{_id: 1});
                if(detail.length > 0 ){
                    return res.status(406).json({
                        sttus: 406,
                        result: false,
                        data: detail,
                        error:{
                            message: `Algunos productos no se han descontado del inventario favor revisar todos los que están resultados con el Check en Azul`
                        }
                    });
                }
                documentRelate = await DocumentRelateModel.aggregate([{
                        $match: { document: ObjectId(id) }
                    },
                    {
                        $group: {
                            _id: '$document',
                            total: { $sum: '$price' },
                            count: { $sum: 1 },
                        }
                    }
                ]);
                if (documentRelate.length <= 0) {
                    return res.status(406).json({
                        status: 406,
                        result: false,
                        total: 0,
                        error: {
                            message: `El Pedido No tiene facturas asociadas, para continuar debe agregar al menos una`
                        }
                    })
                }
                if (parseInt(documentRelate[0].total) > parseInt(total)) {
                    diferencia = documentRelate[0].total - total;
                    return res.status(406).json({
                        status: 406,
                        result: false,
                        total: total,
                        bill: documentRelate[0].total,
                        error: {
                            message: `El valor facturado ${documentRelate[0].total.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) } es mayor al del pedido ${ total.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) } sobra ${ diferencia.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) }`,
                        }
                    })
                } else if (parseInt(documentRelate[0].total) < parseInt(total)) {
                    diferencia = total - documentRelate[0].total;
                    return res.status(406).json({
                        status: 406,
                        result: false,
                        total: total,
                        bill: documentRelate[0].total,
                        error: {
                            message: `El valor facturado ${documentRelate[0].total.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        })} es menor al del pedido ${ total.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) } faltan ${ diferencia.toLocaleString('en', {   
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                        }) }`,
                        }
                    })
                }
            }
            const newRecord = req.body;
            const oldRecord = await Model.findByIdAndUpdate(id, newRecord);
            const actualStatus = await StatusModel.findById(oldRecord.status);
            const newStatus = await StatusModel.findById(newRecord.status);
            message = `<b>Pedido Nro.:</b> ${ oldRecord.number }<br><b>Actual: </b><span class="text-${ actualStatus.color }"><i class="${ actualStatus.icon }"></i> ${ actualStatus.name }</span><br><b>Nuevo: </b><span class="text-${ newStatus.color }"><i class="${ newStatus.icon }"></i> ${ newStatus.name }</span>`;
            await saveLog(200, message, 'document', id, req.user._id, 'Update | Estados');

            return res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    getByStatus: async(req, res, next) => {
        status = await StatusModel.find({ order: { $in: req.body.status } }, { _id: 1 });
        data = await Model.find({
                status: { $in: status }
            }, {
                number: 1,
                date: 1
            })
            .populate({
                path: 'client status user',
                select: 'number name lastname icon color'
            });
        res.status(200).json({
            status: 200,
            result: true,
            data: data,
            total: await Model.count({ status: { $in: status } })
        });
    }
}