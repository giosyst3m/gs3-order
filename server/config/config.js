// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;


// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


// ============================
//  Vencimiento del Token
// ============================
// 60 segundos
// 60 minutos
// 24 horas
// 30 días
process.env.CADUCIDAD_TOKEN = '48h';


// ============================
//  SEED de autenticación
// ============================
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';

// ============================
//  Base de datos
// ============================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    //urlDB = 'mongodb://localhost:27017/gs3-order';
    // urlDB = 'mongodb://gs3-order:caracas01@ds151530.mlab.com:51530/gs3-order';
    urlDB = 'mongodb://oonline:CXXZK790pKHmbEPB@giosyst3m-shard-00-00-s3mcc.mongodb.net:27017,giosyst3m-shard-00-01-s3mcc.mongodb.net:27017,giosyst3m-shard-00-02-s3mcc.mongodb.net:27017/orderonline?ssl=true&replicaSet=GioSyst3m-shard-0&authSource=admin&retryWrites=false';
} else {
    urlDB = process.env.MONGO_URI;
}
process.env.URLDB = urlDB;

// ============================
//  Google Client ID
// ============================
process.env.CLIENT_ID = process.env.CLIENT_ID || '219758474264-vh1bibcphgvbc32km508lubtqkanikf1.apps.googleusercontent.com';

// ============================
//  Cloudinary
// ============================

var cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: 'giosyst3m',
    api_key: '111536255177136',
    api_secret: 'noStxw01OKjGw8yGgQEqLdbnrmo'
});

// ============================
//  Mail
// ============================

process.env.mailFrom = process.env.mailFrom || '"Pedidos OnLine" <sistemas@giosyst3m.net>';
process.env.mailHost = process.env.mailHost || 'md-17.webhostbox.net';
process.env.mailPort = process.env.mailPort || 465;
process.env.mailSecure = process.env.mailSecure || true; // true for 465, false for other ports
process.env.mailUser = process.env.mailUser || 'sistemas@giosyst3m.net' // generated ethereal user;
process.env.mailPass = process.env.mailPass || 'caracas01*' // generated ethereal password

// ============================
//  GS3
// ============================
process.env.sys_email = process.env.sys_email || 'sistemas@giosyst3m.net';
process.env.sys_url = process.env.sys_url || 'http://giosyst3m.net';
process.env.sys_name = process.env.sys_name || 'Pedidos OnLine';
process.env.sys_company = process.env.sys_company || 'GioSyst3m';