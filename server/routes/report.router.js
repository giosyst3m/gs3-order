const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/report.controller`);
const app = express();

app.post('/export/product/excel', [verificaToken, validAccess], (req, res, next) => {
    Controller.productToExcel(req, res, next);
});
app.post('/export/product/pdf/:email/:img', [verificaToken, validAccess], (req, res, next) => {
    Controller.productToPDF(req, res, next);
});

module.exports = app;