const express = require('express');

const fs = require('fs');
const path = require('path');

const { verificaToken, validAccess } = require('../middlewares/autenticacion');


let app = express();


app.get('/img/:table/:file', [verificaToken, validAccess], (req, res) => {


    let table = req.params.table;
    let file = req.params.file;

    let pathImagen = path.resolve(__dirname, `../../uploads/${ table }/${ file }`);

    if (fs.existsSync(pathImagen)) {
        res.sendFile(pathImagen);
    } else {
        let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
        res.sendFile(noImagePath);
    }



});





module.exports = app;