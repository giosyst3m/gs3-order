const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    code: {
        type: String,
        required: [true, 'Código registro']
    },
    message: {
        type: String,
        required: [true, 'Mensaje es requerido']
    },
    entity: {
        type: String,
        required: [true, 'Entidad es requerida']
    },
    id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    type: {
        type: String,
        required: [true, 'Indique el Tipo']
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    date: {
        type: Date,
        default: Date.now
    },
});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('log', ModelSchema);