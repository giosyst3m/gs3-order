const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const nodemailer = require('nodemailer');
const Product = require('../models/product.model');
const Brand = require('../models/brand.model');
const Line = require('../models/line.model');
const Category = require('../models/category.model');
const Type = require('../models/type.model');
const Condition = require('../models/condition.model');
const CityModel = require('../models/city.model');
const StateModel = require('../models/state.model');
const ClientModel = require('../models/client.model');
const ZoneModel = require('../models/zone.model');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const fs = require('fs');
const path = require('path');
var XLSX = require('xlsx');
var cloudinary = require('cloudinary');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

app.put('/upload/import/client/:code', [verificaToken, validAccess, multipartMiddleware], (req, res, next) => {
    if (!req.params.code) {
        return res.status(400)
            .json('Debe Seleccionar La zona');
    }
    if (!req.files) {
        return res.status(400)
            .json('No se ha seleccionado ningún archivo');
    }
    // Obtener el nombre del archivo
    var file = req.files.file;
    var shortName = file.name.split('.');
    var ext = shortName[shortName.length - 1];

    // Valid extention
    var extensions = ['xls', 'xlsx'];

    if (extensions.indexOf(ext) < 0) {
        return res.status(400)
            .json(
                `La extensión del archivo es ${ ext } no es permitido, solo debe subir con extenciones ` + extensions.join(', ')
            );
    }

    cloudinaryRestul = cloudinary.uploader.upload(file.path, async function(result) {
        var buf = fs.readFileSync(file.path);
        var wb = XLSX.read(buf, { type: 'buffer' });

        const table = XLSX.readFile(file.path);
        const sheet = table.Sheets[table.SheetNames[0]];
        var range = XLSX.utils.decode_range(sheet['!ref']);
        i = 1;
        datos = [];
        x = Date();
        html = '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr style="color: #fff;background-color: #337ab7;"><td>#</td><td>RUT</td><td>Sucursal</td><td>Nombre</td><td>Dirección</td><td>Teléfono</td><td>Celular</td><td>Ciudad</td><td>Departamento</td><td>Estatus</td></tr></thead>';
        html = html + '<tbody>';
        console.log('START: \x1b[36mClient\x1b[0m - Upload');
        // Get Zone
        zone = await ZoneModel.findById(req.params.code);
        // Passing all client to false by Zone
        await ClientModel.updateMany({ zone: req.params.code }, { status: false });

        // Check all product come from Excel files to update
        for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
            state = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 6 })].v.match(/\((.*?)\)/);
            city = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 6 })].v.replace(/ *\([^)]*\) */g, "").trim();
            if (!state) {
                state = 'S/D';
            } else {
                state = state[1].trim();
            }
            state = await StateModel.upsert({ name: state }, { name: state, user: req.user._id });
            city = await CityModel.upsert({ name: city }, { name: city, state: state._id, user: req.user._id });
            _client = {
                code: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v + '-' + sheet[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v,
                correlative: (!sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v ? sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v : 0),
                name: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim(),
                address: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 5 })].v.trim(),
                city: city._id,
                phone: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 8 })].v,
                mobil: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 9 })].v,
                zone: req.params.code,
                status: true,
                user: req.user._id
            }
            _clientdata = {
                code: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v + '-' + sheet[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v,
                correlative: (!sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v ? sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v : 0),
                name: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim(),
                address: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 5 })].v.trim(),
                city: city.name,
                state: state.name,
                phone: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 8 })].v,
                mobil: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 9 })].v,
                zone: zone.name,
                status: true ? 'activo' : 'inactivo',
                user: req.user._id
            }
            datos.push(_clientdata);
            client = await ClientModel.upsert({ code: _client.code }, _client);
            // HTML Table for email Body
            html = html + '<tr>';
            html = html + `<td>${ i }</td>`;
            html = html + `<td>${ _client.code }</td>`;
            html = html + `<td>${ _client.correlative }</td>`;
            html = html + `<td>${ _client.name }</td>`;
            html = html + `<td>${ _client.address }</td>`;
            html = html + `<td>${ _client.phone }</td>`;
            html = html + `<td>${ _client.mobil }</td>`;
            html = html + `<td>${ city.name }</td>`;
            html = html + `<td>${ state.name }</td>`;
            html = html + `<td>${ _client.status?'activo':'inactivo' }</td>`;
            html = html + '</tr>';
            i++;
        }
        html = html + '</tbody></table>';
        console.log('END: \x1b[36mClient\x1b[0m - Upload: Total: \x1b[37m\x1b[41m%s\x1b[0m\x1b[40m ', i);
        console.log(x);
        console.log(Date());
        //Send Email to notify Beging process Excel File
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        nodemailer.createTestAccount((err, account) => {

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: process.env.mailHost,
                port: process.env.mailPort,
                secure: process.env.mailSecure, // true for 465, false for other ports
                auth: {
                    user: process.env.mailUser, // generated ethereal user
                    pass: process.env.mailPass // generated ethereal password
                }
            });
            // setup email data with unicode symbols
            let mailOptions = {
                from: process.env.mailFrom, // sender address
                to: req.user.email || process.env.mailFrom, // list of receivers
                subject: 'Importación de Clientes Finalizada', // Subject line
                text: 'Se inicia el Procesos de Actualización de Clientes, ', // plain text body
                html: `<b>Peodidos OnLine</b><p>Se Finalizó el Procesos de Clients de la zona ${ zone.name } , favor revisar el sistema </p> ${ html }` // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId.green);
                // Preview only available when sending through an Ethereal account
                //console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });
        return res.status(200)
            .json({
                ok: true,
                message: 'Arhivo subido',
                data: datos,
                html: html,
                cloudinary: result,
            });
    }, { resource_type: "raw", folder: 'import/client', tags: ['import ', 'client', 'SIGO'] });

});

app.put('/upload/import/product/:code', [verificaToken, validAccess, multipartMiddleware], async(req, res, next) => {
    // var type = req.params.type;

    // Array type of infomation can upload
    // var typeValid = ['product', 'client'];

    // if (typeValid.indexOf(type) < 0) {
    //     return res.status(400)
    //         .json({
    //             ok: false,
    //             err: {
    //                 message: 'Tipo de conecciòn no es valida'
    //             }
    //         });
    // }
    if (!req.files) {
        return res.status(400)
            .json({
                ok: false,
                err: {
                    message: 'No se ha seleccionado ningún archivo'
                }
            });
    }
    // Obtener el nombre del archivo
    var file = req.files.file;
    var shortName = file.name.split('.');
    var ext = shortName[shortName.length - 1];

    // Valid extention
    var extensions = ['xls', 'xlsx'];

    if (extensions.indexOf(ext) < 0) {
        return res.status(400)
            .json({
                ok: false,
                err: {
                    message: `La extensiòón del archivo es ${ ext } no es permitid, solo debe subir ` + extensions.join(', ')
                }
            });
    }


    // Upload in Cloudinary
    cloudinary.uploader.upload(file.path, async function(result) {
        //data = processProductFile(file.path);

        var buf  = fs.readFileSync(file.path);
        var wb = XLSX.read(buf, { type: 'buffer' });

        const table = XLSX.readFile(file.path);
        const sheet = table.Sheets[table.SheetNames[0]];
        var range = XLSX.utils.decode_range(sheet['!ref']);
        datos = [];
        brand = '';
        line = '';
        category = '';
        i = 1;
        x = Date();
        console.log('START: \x1b[36mProduct\x1b[0m - Update');
        // Table html for email
        html = `<table>
                    <thead>
                        <tr style="color: #fff;background-color: #337ab7;">
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Linea</th>
                            <th>sku</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Barcode</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Categoria</th>
                            <th>Condición</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>'`;


        // Put all product inactived 
        await Product.updateMany({}, { status: false });

        console.log('START UPDATE'.green,Date());
        const ProductTMP = require('../models/product.tmp.model');
        await ProductTMP.deleteMany({});
        i=0;
        for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
            i++;
            if( sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim() != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v.trim() != '' &&
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 7 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 8 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 10 })].v != '' && 
                sheet[XLSX.utils.encode_cell({ r: rowNum, c: 16 })].v != '' ){
                if (sheet[XLSX.utils.encode_cell({ r: rowNum, c: 7 })].v > 0) { // Column H - 7 : Quantity
                    status = true;
                } else {
                    status = false;
                }
                // Column B - 1 : Line
                var str = "" + sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v;
                var pad = "0000";
                var code_01 = pad.substring(0, pad.length - str.length) + str;
                // Column C - 2 : Code
                var str = "" + sheet[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v;
                var pad = "000000";
                var code_02 = pad.substring(0, pad.length - str.length) + str;
                // Column A - 0 : Type
                code = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v + code_01 + code_02;
                _productTMP = {
                    excel: i,
                    type: Number(sheet[XLSX.utils.encode_cell({ r: rowNum, c: 0 })].v),
                    line: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v,
                    sku: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v.trim(), // Column D - 3 : SKU
                    name: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v.replace(/ *\([^)]*\) */g, "").trim(), // Column E - 4 : Name with (BRAND)
                    brand: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v.match(/\((.*?)\)/)[1].trim(),
                    barcode: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 10 })].v, // Column K - 10 : Barcode
                    price: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 16 })].v, // Column Q - 16 : Price
                    stock: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 7 })].v, // Column H - 7 : Quantity
                    category: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 8 })].v,
                    img: `product/${ sheet[XLSX.utils.encode_cell({ r: rowNum, c: 10 })].v  }`, // Column K - 10 : Barcode
                    condition: 1,
                    status: status,
                    code: code,
                    user: req.user._id
                };
            product = await ProductTMP.upsert({ barcode: _productTMP.barcode }, _productTMP );
            }else{
                console.log('ERROR'.red, ' Línea Excel ', i);
            }
        }
        
        console.log('END Update'.green,Date());
        products = await ProductTMP.find({});
        types = await Type.find({});
        lines = await Line.find({});
        conditions = await Condition.find({});
        brands = await Brand.find({});
        categories = await Category.find({});
        i = 0;
        for (let index = 0; index < products.length; index++) {
            const productROW = products[index];
                
                // Column A - 0 : Type
                type = await types.find(x => x.code === productROW.type );
                if( type === undefined ){
                    type = await Type.upsert({ code:productROW.type }, { code: productROW.type, user: req.user._id });
                    types = await Type.find({});
                }            
                // Column B - 1 : Line
                line = await lines.find(x => x.code === productROW.line );
                if( line === undefined ){
                    line = await Line.upsert({ code: productROW.line }, { code: productROW.line, user: req.user._id });
                    lines = await Line.find({});
                }                
                // Condition 1
                condition = await conditions.find(x => x.code === productROW.condition );
                if( line === undefined ){
                    condition = await Condition.upsert({ code: productROW.condition }, { code: productROW.condition, user: req.user._id });
                    conditions = await Condition.find({});
                }                
                // Column E - 5: Brand between ( )
                brandUpate = await brands.find(x => x.name === productROW.brand );
                if( brandUpate === undefined ){
                    brandUpate = await Brand.upsert({ name: productROW.brand  }, { name: productROW.brand, user: req.user._id });
                    brands = await Brand.find({});
                }  
                // Column I - 8: Category
                category = await categories.find(x => x.code === productROW.category);
                if( category === undefined ){
                    category = await Category.upsert({ code: productROW.category }, { code: productROW.category, user: req.user._id });
                    categories = await Category.find({});
                }  
   
                _product = {
                    type: type._id,
                    line: line._id,
                    sku: productROW.sku, // Column D - 3 : SKU
                    name: productROW.name, // Column E - 4 : Name with (BRAND)
                    brand: brandUpate._id,
                    barcode: productROW.barcode, // Column K - 10 : Barcode
                    price: productROW.price, // Column Q - 16 : Price
                    stock: productROW.stock, // Column H - 7 : Quantity
                    category: category._id,
                    img: `product/${ productROW.barcode  }`, // Column K - 10 : Barcode
                    condition: condition._id,
                    status: productROW.status,
                    code: productROW.code,
                    user: req.user._id
                };

                datos.push({
                    type: type.name,
                    line: line.name,
                    sku: productROW.sku, // Column D - 3 : SKU
                    name: productROW.name, // Column E - 4 : Name with (BRAND)
                    brand: brandUpate.name,
                    barcode: productROW.barcode, // Column K - 10 : Barcode
                    price: productROW.price, // Column Q - 16 : Price
                    stock: productROW.stock, // Column H - 7 : Quantity
                    category: category.name,
                    condition: condition.name,
                    status: productROW.status ? 'Activo' : 'Inactivo',
                    code: productROW.code,
                    user: req.user._id,
                    img: `product/${ productROW.barcode }`, // Column K - 10 : Barcode
                });
                
                product = await Product.upsert({ barcode: _product.barcode }, _product );
                
                // HTML Table for email Body
                html += '<tr>';
                html += `<td>${ i++ }</td>`;
                html += `<td>${ type.code } - ${ type.name }</td>`;
                html += `<td>${ line.code } - ${ line.name }</td>`;
                html += `<td>${ productROW.sku }</td>`;
                html += `<td>${ productROW.name }</td>`;
                html += `<td>${ brandUpate.name }</td>`;
                html += `<td>${ productROW.barcode }</td>`;
                html += `<td style="text-align: right">${ productROW.price }</td>`; 
                html += `<td>${ productROW.stock }</td>`;
                html += `<td>${ productROW.category }</td>`;
                html += `<td>${ category.code } - ${ category.name }</td>`;
                html += `<td>${ condition.code } - ${ condition.name }</td>`;
                html += `<td>${ productROW.status ?'Activo':'Inactivo' }</td>`;
                html += '</tr>';
            }
        html = html + '</tbody></table>';

        //Send Email to notify Beging process Excel File
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        nodemailer.createTestAccount((err, account) => {

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: process.env.mailHost,
                port: process.env.mailPort,
                secure: process.env.mailSecure, // true for 465, false for other ports
                auth: {
                    user: process.env.mailUser, // generated ethereal user
                    pass: process.env.mailPass // generated ethereal password
                }
            });
            // setup email data with unicode symbols
            let mailOptions = {
                from: process.env.mailFrom, // sender address
                to: req.user.email || process.env.mailFrom, // list of receivers
                subject: 'Importación de Productos Finalizada', // Subject line
                text: 'Se inicia el Procesos de Productos y actualización de inventario, ', // plain text body
                html: '<b>Peodidos OnLine</b><p>Se Finalizó el Procesos de Productos y actualización de inventario, favor revisar el sistema </p>' + html // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId.green);
                // Preview only available when sending through an Ethereal account
                // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });
        console.log('END: \x1b[36mProduct\x1b[0m - Update \x1b[37m\x1b[41m%s\x1b[0m\x1b[40m ', i);
        console.log(x);
        console.log(Date());
        return res.status(200)
            .json({
                ok: true,
                message: 'Arhivo subido',
                cloudinary: result,
                data: datos
            });
    }, { resource_type: "raw", folder: 'import/product', tags: ['import ', 'product', 'SIGO'] });

});

app.put('/upload/img/:tipo/:id', [verificaToken, validAccess, multipartMiddleware], async(req, res) => {

    let tipo = req.params.tipo;
    let id = req.params.id;

    if (!req.files) {
        return res.status(400)
            .json({
                ok: false,
                err: {
                    message: 'No se ha seleccionado ningún archivo'
                }
            });
    }

    // Valida tipo
    let tiposValidos = ['product', 'user'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(500).json({
            ok: false,
            err: {
                message: 'Los tipos permitidas son ' + tiposValidos.join(', ')
            }
        })
    }

    let archivo = req.files.imagen;
    let nombreCortado = archivo.name.split('.');
    let extension = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg', 'JPG', 'PNG'];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Las extensiones permitidas son ' + extensionesValidas.join(', '),
                ext: extension
            }
        })
    }

    let Entity = require(`../models/${ tipo }.model`);

    data = await Entity.findById(id);
    if (!data) {
        return res.status(400)
            .json({
                status: 400,
                ok: true,
                error: {
                    message: 'No se encontra dato relacionado'
                },
            });
    }

    if (tipo == 'user') {
        filenName = data.email + '-' + Math.random(2);
        fileDelete = data.avatar;
    } else if (tipo == 'product') {
        filenName = data.barcode;
        fileDelete = data.img;
    }

    // Delete old filte
    if (fileDelete != 'avatar' && fileDelete != 'no-imagen') {
        await cloudinary.uploader.destroy(fileDelete);
    }

    // Upload in Cloudinary
    cloudinary.uploader.upload(archivo.path, async(result) => {
        if (tipo == 'user') {
            await Entity.findByIdAndUpdate(id, { avatar: result.public_id });
        } else if (tipo == 'product') {
            await Entity.findByIdAndUpdate(id, { img: result.public_id, img_url: result.secure_url });
        }
        console.log(`Update Record: ${ id.cyan } Entity: ${ tipo.green }`);
        return res.status(200)
            .json({
                ok: true,
                message: 'Arhivo subido',
                cloudinary: result,
                img: result.public_id
            });
    }, { folder: tipo, tags: [tipo], public_id: filenName });

});

function processProductFile(data) {
    console.log('Procesando archivo de excel');
    var buf = fs.readFileSync(data);
    var wb = XLSX.read(buf, { type: 'buffer' });

    const table = XLSX.readFile(data);
    const sheet = table.Sheets[table.SheetNames[0]];
    var range = XLSX.utils.decode_range(sheet['!ref']);
    datos = [];
    for (let rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
        // Example: Get second cell in each row, i.e. Column "A = 0, B = 1; C = 3... Z = N"
        // const secondCell = sheet[XLSX.utils.encode_cell({ r: rowNum, c: 7 })];
        // NOTE: secondCell is undefined if it does not exist (i.e. if its empty)
        // console.log(secondCell); // secondCell.v contains the value, i.e. string or number
        datos.push({
            type: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 1 })].v,
            line: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 2 })].v,
            sku: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 3 })].v,
            name: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v,
            brand: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v.match(/\((.*?)\)/)[1].trim(),
            barcode: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 5 })].v,
            price: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 7 })].v,
            qty: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 8 })].v,
            category: sheet[XLSX.utils.encode_cell({ r: rowNum, c: 10 })].v,
            brands: processProduct(sheet[XLSX.utils.encode_cell({ r: rowNum, c: 4 })].v.match(/\((.*?)\)/)[1].trim())
        });

    }
    console.log('Fin archivo de excel');
    return datos;
}




function processProduct(brand) {
    // console.log(brand);
    return Brand.findOne({ name: brand })
        .exec((err, res) => {
            if (!res) {
                return 'no';
            } else {
                console.log('SI');
                return res;
            }
        });
    // return brand;
}

module.exports = app;