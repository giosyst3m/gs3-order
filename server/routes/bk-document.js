const express = require('express');
const { verificaToken } = require('../middlewares/autenticacion');


let app = express();
let Document = require('../models/document');
require('../models/client');
let Type = require('../models/type');
let Status = require('../models/status');
let Counter = require('../models/counter');
let Company = require('../models/company');
let Product = require('../models/product');
let Detail = require('../models/detail');
let Brand = require('../models/brand');
let State = require('../models/state');
let Discount = require('../models/discount');
let Delivery = require('../models/delivery');
let Tax = require('../models/tax');
let DocumentRelate = require('../models/documentrelate');

// ===========================
//  Search Order by Client, Type and Status
// ===========================
app.get('/document/client/:client/:type/:status/:company', (req, res) => {

    Type.findOne({ name: req.params.type, table: 'document' })
        .exec((err, data) => {

            if (err) {
                return res.status(500).json({
                    status: 500,
                    result: false,
                    err: err
                });
            }

            if (!data) {
                return res.status(404).json({
                    status: 404,
                    result: false,
                    err: {
                        mensaje: `No se encontro el Tipoe de documento "${ req.params.type }" para el Cliente`
                    }
                });


            }

            Status.findOne({ order: req.params.status, })
                .exec((err, datos) => {
                    if (err) {
                        return res.status(500).json({
                            status: 500,
                            result: false,
                            err: err
                        });
                    }

                    if (!datos) {
                        return res.status(404).json({
                            status: 404,
                            result: false,
                            err: {
                                mensaje: `No se encontro el Estatus "${ req.params.status }" para el Cliente`
                            }
                        });


                    }

                    Document.findOne({
                            client: { _id: req.params.client },
                            type: { _id: data._id },
                            status: { _id: datos._id },
                            company: { _id: req.params.company }
                        })
                        .populate('status')
                        .populate('type')
                        .populate('client')
                        .exec((err, row) => {
                            if (err) {
                                return res.status(500).json({
                                    status: 500,
                                    result: false,
                                    err: err
                                });
                            }

                            if (!row) {
                                return res.status(404).json({
                                    status: 404,
                                    result: false,
                                    message: `No se encontro Pedido con estatus "${ datos.name }" para el cliente`

                                });


                            }

                            return res.json({
                                status: 200,
                                result: true,
                                data: row
                            });

                        });

                });

        });


});

// ============================
// Create a haeder
// ============================
app.post('/document/header/:type/:status', function(req, res) {

    Type.findOne({ status: true, name: req.params.type })
        .exec((err, datos) => {
            if (err) {
                return res.status(500).json({
                    status: 500,
                    result: false,
                    err
                });
            }
            if (!datos) {
                return res.status(404).json({
                    status: 404,
                    result: false,
                    err: {
                        mensaje: `No se encontro el Tipoe de documento "${ req.params.type }" para el Cliente`
                    }
                });


            }
            req.body['type'] = datos._id;
            Status.findOne({ name: req.params.status })
                .exec((errS, dataS) => {
                    if (errS) {
                        return res.status(500).json({
                            status: 500,
                            result: false,
                            errS
                        });
                    }
                    if (!datos) {
                        return res.status(404).json({
                            status: 404,
                            result: false,
                            err: {
                                mensaje: `No se encontro el Estatus "${ req.params.status }" para el Cliente`
                            }
                        });
                    }
                    req.body['status'] = dataS._id;
                    Counter.findOne({ type: { _id: req.body.type }, company: { _id: req.body.company } })
                        .exec((err, dataC) => {
                            req.body['number'] = dataC.number + 1;
                            Counter.findByIdAndUpdate(dataC._id, { number: req.body.number }, { new: true, runValidators: true }, (err, dataUC) => {
                                let Model = new Document(req.body);
                                Model.save((errors, data) => {
                                    if (errors) {
                                        return res.status(400).json({
                                            status: 400,
                                            result: false,
                                            errors
                                        });
                                    }
                                    res.json({
                                        status: 200,
                                        result: true,
                                        data
                                    });
                                });
                            });
                        });


                });

        });
});
// ============================
// Create a Detail
// ============================
app.post('/document/detail', function(req, res) {
    Product.findById(req.body.product, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: 500,
                result: false,
                err
            });
        }
        if (!data) {
            return res.status(500).json({
                status: 500,
                result: false,
                err: {
                    message: 'El ID no es correcto'
                }
            });
        }
        req.body['price'] = data.price;
        let Model = new Detail(req.body);
        Model.save((errors, dataD) => {
            if (errors) {
                return res.status(400).json({
                    status: 400,
                    result: false,
                    errors
                });
            }
            res.json({
                status: 200,
                result: true,
                data: dataD
            });
        });
    });

});

// ============================
// Gell Detail from ID
// ============================
app.get('/document/detail/:id', (req, res) => {

    let limit = Number(req.query.limit || 10);
    let skip = Number(req.query.from || 0);

    Detail.find({ document: { _id: req.params.id }, status: true })
        .sort({ date: 'desc' })
        .limit(limit)
        .skip(skip)
        .populate({
            path: 'product document',
            populate: {
                path: 'brand category type line conditional'
            }
        })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: 400,
                    result: false,
                    err
                });
            }
            Detail.count({ document: { _id: req.params.id }, status: true }, (err, total) => {
                res.json({
                    status: 200,
                    result: true,
                    data,
                    total
                });
            });


        })
});

// ============================
// Get Document By ID
// ============================
app.get('/document/:id', (req, res) => {

    Document.findOne({ _id: req.params.id })
        .populate({
            path: 'client status tax type discount delivery',
            populate: {
                path: 'category city zone buttom',
                populate: {
                    path: 'state'
                }

            }
        })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: 400,
                    result: false,
                });
            }

            res.json({
                status: 200,
                result: true,
                data
            });

        })
});

// ============================
// Get all rows
// ============================
app.get('/document/datatables/:id', (req, res) => {
    let limit = Number(req.query.limit || 15);
    let skip = Number(req.query.from || 0);
    Detail.find({ document: req.params.id })
        .limit(limit)
        .skip(skip)
        .populate({
            path: 'product',
            populate: {
                path: 'brand line type condition'
            },
            options: {
                sort: {
                    barcode: 'descending'
                }
            }
        })
        .exec((errors, data) => {
            if (errors) {
                return res.status(500).json({
                    status: 500,
                    result: false,
                    errors
                });
            }
            Detail.count({ document: req.params.id })
                .exec((err, total) => {
                    res.json({
                        status: 200,
                        result: false,
                        total,
                        data
                    });
                });

        });
});

// ============================
// Get all rows
// ============================
app.get('/document/totals/:id', (req, res) => {
    Detail.find({ document: req.params.id })
        .populate({
            path: 'product',
            populate: {
                path: 'brand line type condition'
            },
        })
        .exec((errors, data) => {
            if (errors) {
                return res.status(500).json({
                    status: 500,
                    result: false,
                    errors
                });
            }
            let qty = 0;
            let sum = 0;

            for (const iterator of data) {
                qty = qty + iterator.quantity;
                sum = sum + (iterator.quantity * iterator.price);
            }

            Document.findOne({ _id: req.params.id })
                .populate('tax')
                .populate('discount')
                .populate('delivery')
                .exec((err, doc) => {
                    let discount = 0;
                    let total = 0;
                    let delivery = 0;
                    let tax = 0;
                    if (err) {
                        return res.status(500).json({
                            status: 500,
                            result: false,
                            errrs
                        });
                    }

                    if (doc.discount) {
                        discount = (doc.discount.discount / 100) * sum;
                    }

                    total = sum - discount;

                    for (const iterator of doc.delivery) {
                        delivery = delivery + iterator.price;
                    }

                    total = total + delivery;

                    for (const iterator of doc.tax) {
                        tax = tax + ((iterator.tax / 100) * total);
                    }

                    total = total + tax;

                    res.json({
                        status: 200,
                        result: true,
                        data: {
                            quantity: qty,
                            sum: sum,
                            tax: tax,
                            delivery: delivery,
                            discount: discount,
                            total: total
                        },

                    });
                });

        });
});

// ============================
// Delete all Deatil are in 0
// ============================
app.post('/document/detail/inZero', (req, res) => {

    for (let index = 0; index < req.body.length; index++) {
        const element = req.body[index];

        Detail.findByIdAndRemove(element, (err, data) => {
            res.json({
                status: 200,
                result: true,
                message: `Los registros con el ID ${ element } fueron eliminado`
            });
        });

    }
});

// ============================
// Delete all Deatil No take inventory
// ============================
app.delete('/document/detail/NoInventory/:document', (req, res) => {
    Detail.count({ document: req.params.document, inventory: false }, (err, total) => {
        if (err) {
            return res.status(500).json({
                status: 500,
                result: false,
                err
            });
        }

        if (total > 0) {
            Detail.find({ document: req.params.document, inventory: false })
                .exec((err, det) => {
                    for (let index = 0; index < det.length; index++) {
                        const element = det[index]['_id'];
                        Detail.findByIdAndRemove(element, (err, resp) => {
                            if (err) {
                                return res.status(500).json({
                                    status: 500,
                                    result: false,
                                    err
                                });
                            }
                        });
                    }

                    res.json({
                        status: 200,
                        result: true,
                        data: total
                    });
                });
        }
    });
});

// ============================
// Create a row Document Relate by Document
// ============================
app.post('/document/relate/:document', function(req, res) {

    let body = req.body;
    body['document'] = req.params.document;
    let Model = new DocumentRelate(body);

    Model.save((errors, data) => {
        if (errors) {
            return res.status(400).json({
                status: 400,
                result: false,
                errors
            });
        }

        res.json({
            status: 200,
            result: true,
            data
        });


    });


});
// ============================
// Get all rows
// ============================
app.get('/document/relate/:document', (req, res) => {


    DocumentRelate.find({ document: req.params.document })
        .exec((err, data) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                data
            });

        })
});


module.exports = app;