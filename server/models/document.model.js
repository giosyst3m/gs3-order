const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    number: {
        type: Number,
        required: true
    },
    client: {
        type: Schema.Types.ObjectId,
        ref: 'Client',
        required: true
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: true
    },
    discount: {
        type: Schema.Types.ObjectId,
        ref: 'Discount',
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    counter: {
        type: Schema.Types.ObjectId,
        ref: 'Counter',
        required: true
    },
    note: {
        type: String
    },
    delivery: [{
        type: Schema.Types.ObjectId,
        ref: 'Delivery',
    }],
    tax: [{
        type: Schema.Types.ObjectId,
        ref: 'Tax',
        required: true
    }],
    date: {
        type: Date,
        default: Date.now
    },
    pay_date: {
        type: Date,
        required: false,
    },
    pay_total: {
        type: Number,
        required: false,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Document', ModelSchema);