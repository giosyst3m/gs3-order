module.exports = {
    /**
     * @param model of entity
     * @param from number from record wants to start
     * @param value string value to search
     * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
     * @param status number Get Records 1 = Active 2 = Inactive 3 = both
     * @return object {Status, Result, Data, TotalRecords, [error]}
     */
    getAll: async(req, res, next, Model) => {
        try {
            let limit = Number(req.query.limit || 10);
            let skip = Number(req.query.from || 0);
            if (req.query.value.length > 0) {
                filters = { $text: { $search: req.query.value } };
            } else {
                filters = {};
            }

            if (req.query.status == 1) {
                filters.status = true;
            } else if (req.query.status == 2) {
                filters.status = false;
            }
            const data = await Model.find(filters)
                .limit(limit)
                .skip(skip);

            res.status(200).json({
                status: 200,
                result: true,
                total: await (Model.count(filters)),
                data: data,
            });

        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },
    /**
     * Active Record from Entity by ID
     * @param id string name
     * @param model of entity
     * @return object {Status, Result, Data, [error]}
     */
    active: async(req, res, next, Model) => {
        try {
            const { id } = req.params;
            const oldRecord = await Model.findByIdAndUpdate(id, { status: true }, { new: true, runValidators: true });
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    /**
     * Inacctive Record from Entity by ID
     * @param id string name
     * @param model of entity
     * @return object {Status, Result, Data, [error]}
     */
    inactive: async(req, res, next, Model) => {
        try {
            const { id } = req.params;
            const oldRecord = await Model.findByIdAndUpdate(id, { status: false }, { new: true, runValidators: true });
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    /**
     * Create a record from table
     * @param model of entity
     * @param body array data
     * @return object {Status, Result, Data, [error]}
     */
    create: async(req, res, next, Model) => {
        try {
            req.body['user'] = req.user._id;
            const newRecord = new Model(req.body);
            const record = await newRecord.save();
            res.status(200).json({
                status: 200,
                result: true,
                data: record
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    /**
     * Update a record from table by Id
     * @param model of entity
     * @param id string 
     * @param body array data
     * @return object {Status, Result, Data, [error]}
     */
    update: async(req, res, next, Model) => {
        try {
            const { id } = req.params;
            const newRecord = req.body;
            const oldRecord = await Model.findByIdAndUpdate(id, newRecord);
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },
    /**
     * Get a record from entity by Id
     * @param model of entity
     * @param id strign of record
     * @return object {Status, Result, Data, [error]}
     */
    getById: async(req, res, next, Model) => {
        try {
            const { id } = req.params;
            const data = await Model.findById(id,{user: 0});
            if (!Model) {
                res.status(400).json({
                    status: 400,
                    result: false,
                    err: {
                        message: 'ID no existe'
                    }
                });
            } else {
                res.status(200).json({
                    status: 200,
                    result: true,
                    data: data
                });
            }
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }

    },
    /**
     * Inacctive Record from Entity by ID
     * @param id string name
     * @param model of entity
     * @return object {Status, Result, Data, [error]}
     */
    delete: async(req, res, next, Model) => {
        try {
            const { id } = req.params;
            const oldRecord = await Model.findByIdAndRemove(id);
            res.status(200).json({
                status: 200,
                result: true,
                data: oldRecord
            });
        } catch (err) {
            res.status(500).json({
                status: 500,
                result: false,
                err: err
            });
        }
    },

}