const nodemailer = require('nodemailer');
var colors = require('colors');

/**
 * create reusable transporter object using the default SMTP transport
 */
let transporter = nodemailer.createTransport({
    host: process.env.mailHost,
    port: process.env.mailPort,
    secure: process.env.mailSecure, // true for 465, false for other ports
    auth: {
        user: process.env.mailUser, // generated ethereal user
        pass: process.env.mailPass // generated ethereal password
    }
});

/**
 * Send email by setting up
 * @param {array} mailOptions Config to send email
 */
let sendEmail = (mailOptions) => {
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId.green);
        // Preview only available when sending through an Ethereal account
        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

    });
}

/**
 * Send a email to user when has been created
 * @param {data} req array with user information
 */
let sendEmailUserCreate = (req) => {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // setup email data with unicode symbols
        let mailOptions = {
            from: process.env.mailFrom, // sender address
            to: req.body.email, // list of receivers
            subject: 'Binvenido a Pedidos On Line', // Subject line
            html: `Bienvenidos a ${ req.body.name }, ${ req.body.lastname }<br> Su usuario: ${ req.body.email } <br> Contraseña: ${ password }<br> ${ req.body.role }` // html body
        };
        sendEmail(mailOptions);

    });
};

/**
 * Send a email to user has been changed its password.
 * @param {user} user String User data
 * @param {Password} password strgin new password
 */
let sendEmailReseatPassword = (user, password, subj = 'Cambio') => {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // setup email data with unicode symbols
        let mailOptions = {
            from: process.env.mailFrom, // sender address
            to: user.email, // list of receivers
            subject: `${subj } de contraseña`, // Subject line
            html: `Hola ${ user.name } ${ user.lastname }<br> Su contraseña ha sido cambiada <br> Contraseña: ${ password }` // html body
        };
        sendEmail(mailOptions);

    });
}

let sendEmailDocPDF = (user, email, link, subj = 'Descarga') => {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // setup email data with unicode symbols
        let mailOptions = {
            from: process.env.mailFrom, // sender address
            to: email, // list of receivers
            subject: `PDF: ${ subj } `, // Subject line
            html: `Hola ${ email }, <br> ${ user.name } ${ user.lastname } te ha enviado un link <a href="${ link }">Clic aquí para descargar PDF</a> ` // html body
        };
        sendEmail(mailOptions);

    });
}

module.exports = {
    sendEmailUserCreate,
    sendEmailReseatPassword,
    sendEmailDocPDF
}