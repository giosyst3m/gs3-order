const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ModelSchema = new Schema({
    message: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    row: {
        type: String,
        required: true
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ name: 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Comment', ModelSchema);