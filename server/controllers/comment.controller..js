module.exports = {
    /**
     * @param model of Comment
     * @param from number from record wants to start
     * @param value string value to search
     * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
     * @param status number Get Records 1 = Active 2 = Inactive 3 = both
     * @return object {Status, Result, Data, TotalRecords, [error]}
     */
    getAll: async(req, res, next, Model) => {
        try {
            let limit = Number(req.query.limit || 5);
            let skip = Number(req.query.from || 0);
            Model.find({ type: req.params.type, row: req.params.row })
                .populate('user', 'name lastname avatar')
                .sort({ date: 'desc' })
                .limit(limit)
                .skip(skip)
                .exec((err, data) => {
                    if (err) {
                        return res.status(500).json({
                            status: 500,
                            result: false,
                            err
                        });
                    }
                    Model.count({ type: req.params.type, row: req.params.row })
                        .exec((err, total) => {
                            res.json({
                                status: 200,
                                result: false,
                                total,
                                data
                            });
                        });
                });

        } catch (error) {
            res.status(500).json({
                status: 500,
                result: false,
                err: error
            });
        }
    },
}