const mongoose = require('mongoose');
const Schema = mongoose.Schema;

ModelSchema = new Schema({
    sku: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    stock: {
        type: Number,
        default: 0
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'Type',
        required: false
    },
    brand: {
        type: Schema.Types.ObjectId,
        ref: 'Brand',
        required: true
    },
    barcode: {
        type: String,
    },
    condition: {
        type: Schema.Types.ObjectId,
        ref: 'Condition',
        required: false
    },
    line: {
        type: Schema.Types.ObjectId,
        ref: 'Line',
        required: true
    },
    code: {
        type: String
    },
    sticker_sell: {
        type: String
    },
    sticker_color: {
        type: String
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    img: {
        type: String,
        required: false,
        default: 'no-image.jpg'
    },
    img_url: {
        type: String,
        required: false,
        default: 'https://res.cloudinary.com/giosyst3m/image/upload/v1534113067/no-image.jpg'
    },
    status: {
        type: Number,
        required: true,
        default: 1
    },
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

ModelSchema.index({ "$**": 'text' }, { default_language: "spanish" });

module.exports = mongoose.model('Product', ModelSchema);