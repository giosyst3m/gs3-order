const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = mongoose.model('Discount', new Schema({
    name: {
        type: String,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    status: {
        type: Number,
        required: true,
        default: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },


}));