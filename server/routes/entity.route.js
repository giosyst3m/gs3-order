const express = require('express');
const { verificaToken, validAccess } = require('../middlewares/autenticacion');
const Controller = require(`../controllers/entity.controller`);
const app = express();

app.get('/entity/:table', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.getAll(req, res, next, model);
});

app.post('/entity/:table/', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.create(req, res, next, model);
});

app.put('/entity/:table/:id', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.update(req, res, next, model);
});

app.get('/entity/:table/:id', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.getById(req, res, next, model);
});

app.delete('/entity/:table/:id', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.delete(req, res, next, model);
});

app.patch('/entity/active/:table/:id', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.active(req, res, next, model);
});

app.patch('/entity/inactive/:table/:id', [verificaToken, validAccess], (req, res, next) => {
    let model = require(`../models/${ req.params.table }.model`);
    Controller.inactive(req, res, next, model);
});

module.exports = app;






module.exports = app;