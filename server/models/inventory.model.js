const mongoose = require('mongoose')
const Schema = mongoose.Schema;

module.exports = mongoose.model('Inventory', new Schema({
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    required: {
        type: Number,
        required: false,
        default: 0
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    detail: {
        type: Schema.Types.ObjectId,
        ref: 'Deatil',
        required: false
    },
    document: {
        type: Schema.Types.ObjectId,
        ref: 'Document',
        required: false
    },
    type: {
        type: Boolean,
        required: true
    },
    origin: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'Status',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

}));