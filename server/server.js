require('./config/config');
var sslRedirect = require('heroku-ssl-redirect');

const express = require('express');
const mongooseRecursiveUpsert = require('mongoose-recursive-upsert');
const mongoose = require('mongoose');

mongoose.plugin(mongooseRecursiveUpsert);
const path = require('path');
const app = express();
// app.use(sslRedirect());
// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS, PATCH");
    next();
});

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

// habilitar la carpeta public
app.use(express.static(path.resolve(__dirname, '../public')));


// Configuración global de rutas
app.use(require('./routes/index'));

// catch 404 Errors and Forward them to Error Handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Error Handler Function
app.use((err, req, res, next) => {
    const error = app.get('env') === 'development' ? err : {};
    const status = error.status || 500;

    // response to client
    res.status(status).json({
        error: {
            message: error.message
        }
    });

    // show in console
    console.error(err);
});

mongoose.connect(process.env.URLDB, (err, res) => {

    if (err) throw err;

    console.log('Base de datos: \x1b[36m%s\x1b[0m', 'ONLINE');

});



app.listen(process.env.PORT, () => {
    console.log('Escuchando puerto: \x1b[36m%s\x1b[0m', process.env.PORT);
});